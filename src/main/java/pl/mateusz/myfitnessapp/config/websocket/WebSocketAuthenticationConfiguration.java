package pl.mateusz.myfitnessapp.config.websocket;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.messaging.Message;
import org.springframework.messaging.MessageChannel;
import org.springframework.messaging.simp.config.ChannelRegistration;
import org.springframework.messaging.simp.stomp.StompCommand;
import org.springframework.messaging.simp.stomp.StompHeaderAccessor;
import org.springframework.messaging.support.ChannelInterceptor;
import org.springframework.messaging.support.MessageHeaderAccessor;
import org.springframework.security.core.Authentication;
import org.springframework.util.StringUtils;
import org.springframework.web.socket.config.annotation.EnableWebSocketMessageBroker;
import org.springframework.web.socket.config.annotation.WebSocketMessageBrokerConfigurer;
import pl.mateusz.myfitnessapp.security.JWT.JwtProvider;
import java.util.List;

@Configuration
@EnableWebSocketMessageBroker
@Order(Ordered.HIGHEST_PRECEDENCE+99)
@Slf4j
@AllArgsConstructor
public class WebSocketAuthenticationConfiguration implements WebSocketMessageBrokerConfigurer {

    private final JwtProvider jwtProvider;


    @Override
    public void configureClientInboundChannel(ChannelRegistration registration) {
        registration.interceptors(new ChannelInterceptor() {
            @Override
            public Message<?> preSend(Message<?> message, MessageChannel channel) {
                StompHeaderAccessor accessor =
                        MessageHeaderAccessor.getAccessor(message, StompHeaderAccessor.class);
                if (StompCommand.CONNECT.equals(accessor.getCommand())) {
                    List<String> authorization=accessor
                            .getNativeHeader("X-Authorization");
                    log.info("X-Authorization: {}",authorization);

                    String bearerToken=authorization.get(0);

                    if(StringUtils.hasText(bearerToken) && bearerToken.startsWith("Bearer ")){
                        bearerToken=bearerToken.substring(7);
                    }
                    Authentication authentication= jwtProvider.getAuthenticationFromToken(bearerToken);

                    accessor.setUser(authentication);
                }
                return message;
            }
        });
    }
}



