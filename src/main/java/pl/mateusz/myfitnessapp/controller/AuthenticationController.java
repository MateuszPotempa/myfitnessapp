package pl.mateusz.myfitnessapp.controller;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;

import org.springframework.web.bind.annotation.*;
import pl.mateusz.myfitnessapp.dto.authentication.AuthenticationResponse;
import pl.mateusz.myfitnessapp.dto.authentication.LoginRequest;
import pl.mateusz.myfitnessapp.dto.authentication.RefreshTokenRequest;
import pl.mateusz.myfitnessapp.dto.authentication.RegisterRequest;
import pl.mateusz.myfitnessapp.service.AuthenticationService;

import javax.mail.MessagingException;
import javax.validation.Valid;

@RestController
@RequestMapping("/api/auth")
@AllArgsConstructor
@Slf4j
public class AuthenticationController {

    private final AuthenticationService authenticationService;

    @PostMapping(path = "/signup",consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<String>signup(@Valid @RequestBody RegisterRequest registerRequest) throws MessagingException {
        authenticationService.signup(registerRequest);
        log.info(registerRequest.getUserLogin()+ " created");
        return new ResponseEntity<>("User Registration Successful", HttpStatus.CREATED);
    }
    @GetMapping(path = "/accountVerification/{activationToken}")
    public ResponseEntity<String> verifyAccount(@PathVariable("activationToken") String activationToken) {
        authenticationService.verifyAccount(activationToken);
        return new ResponseEntity<>("Account Activated Successully", HttpStatus.OK);
    }


    @PostMapping(path = "/refreshToken")
    public AuthenticationResponse refreshToken(@Valid @RequestBody RefreshTokenRequest refreshTokenRequest) {
        return authenticationService.refreshToken(refreshTokenRequest);
    }

    @PostMapping(path = "/login",consumes = MediaType.APPLICATION_JSON_VALUE)
    public AuthenticationResponse login(@RequestBody LoginRequest loginRequest) {
        log.info("1. User provided " +loginRequest.getEmail()+ " as a form of login");
        return authenticationService.login(loginRequest);
    }

    @GetMapping("/test1")
    public String test1() {
        return "<h1>Hello</h1>";
    }
}
