package pl.mateusz.myfitnessapp.controller;

import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import pl.mateusz.myfitnessapp.dto.comment.CommentRequest;
import pl.mateusz.myfitnessapp.dto.comment.CommentResponse;
import pl.mateusz.myfitnessapp.service.CommentServiceImpl;

import java.io.IOException;

@RestController
@RequestMapping("api/comment")
@AllArgsConstructor
public class CommentController {

    private final CommentServiceImpl commentService;

    @PostMapping(path = "/create", consumes = MediaType.APPLICATION_JSON_VALUE)
    @ResponseStatus(HttpStatus.CREATED)
    public ResponseEntity<String> createComment(@RequestBody CommentRequest commentRequest) throws IOException {
        commentService.createComment(commentRequest);
        return new ResponseEntity<>("Comment created successfully!", HttpStatus.CREATED);
    }

    @PutMapping(path = "/update/{id}", consumes = MediaType.APPLICATION_JSON_VALUE)
    @ResponseStatus(HttpStatus.OK)
    public void updateComment(@RequestBody CommentRequest commentRequest,@PathVariable("id")Long id) {
        commentService.updateComment(commentRequest,id);
    }

    @GetMapping( path = "/{id}",produces = MediaType.APPLICATION_JSON_VALUE)
    public CommentResponse getCommentById(@PathVariable Long id) {
        return commentService.getCommentById(id);
    }

    @DeleteMapping(path = "/{id}")
    public ResponseEntity<String> deleteComment(@PathVariable("id") Long id){
        commentService.deleteComment(id);
        return new ResponseEntity<>("Comment deleted", HttpStatus.OK);
    }
}
