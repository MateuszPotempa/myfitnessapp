package pl.mateusz.myfitnessapp.controller;


import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import pl.mateusz.myfitnessapp.dto.group.UserGroupPayload;
import pl.mateusz.myfitnessapp.service.GroupAdminService;

import javax.validation.Valid;

@RestController
@RequestMapping("group/admin")
@AllArgsConstructor
public class GroupAdminController {


    private final GroupAdminService groupAdminService;


    @PutMapping(path = "/updateRole/{groupId}", consumes = MediaType.APPLICATION_JSON_VALUE)
    @ResponseStatus(HttpStatus.OK)
    @PreAuthorize("@groupPermissionEvaluator.hasGroupAdminRole(principal,#groupId)")
    public void editMemberRole(@PathVariable("groupId")Long groupId,@Valid @RequestBody UserGroupPayload userGroupPayload) {
        groupAdminService.updateMemberRole(groupId,userGroupPayload);
    }

    @PutMapping(path = "/addMember/{groupId}",consumes = MediaType.APPLICATION_JSON_VALUE)
    @ResponseStatus(HttpStatus.OK)
    @PreAuthorize("@groupPermissionEvaluator.hasGroupAdminRole(principal,#groupId)")
    public void addMemberToGroup(@PathVariable("groupId")Long groupId,@Valid @RequestBody UserGroupPayload userGroupPayload){
        groupAdminService.addMemberToGroup(groupId,userGroupPayload);

    }


}
