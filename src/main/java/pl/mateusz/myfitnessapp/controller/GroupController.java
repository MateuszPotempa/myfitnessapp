package pl.mateusz.myfitnessapp.controller;


import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import pl.mateusz.myfitnessapp.dto.group.GroupDto;
import pl.mateusz.myfitnessapp.dto.group.GroupResponse;
import pl.mateusz.myfitnessapp.service.GroupServiceImpl;

@RestController
@RequestMapping("api/group")
@AllArgsConstructor
public class GroupController {

    private final GroupServiceImpl groupService;

    @PostMapping(path = "/create",consumes = MediaType.APPLICATION_JSON_VALUE)
    @ResponseStatus(HttpStatus.CREATED)
    private void createGroup(@RequestBody GroupDto groupDto){
        groupService.createGroup(groupDto);
    }

    @GetMapping( path = "/{id}",produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseStatus(HttpStatus.OK)
    public GroupResponse getGroupById(@PathVariable Long id) {
        return groupService.getGroupById(id);
    }


    
}
