package pl.mateusz.myfitnessapp.controller;


import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import pl.mateusz.myfitnessapp.util.ImageUploadUtil;

import java.io.IOException;

@RestController
@RequestMapping("api/image")
@AllArgsConstructor
public class ImageController {

    private final ImageUploadUtil imageUploadUtil;


    @PostMapping(path = "/upload", consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
    public ResponseEntity<String> uploadImage (@RequestPart(value = "image",required = false) MultipartFile image) throws IOException {
        return new ResponseEntity<>("Path to uploaded image: "+ imageUploadUtil.saveImage(image), HttpStatus.CREATED);
    }
}
