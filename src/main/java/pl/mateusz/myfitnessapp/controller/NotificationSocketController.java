package pl.mateusz.myfitnessapp.controller;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import pl.mateusz.myfitnessapp.model.Notification;
import pl.mateusz.myfitnessapp.service.GroupServiceImpl;

import java.security.Principal;
import java.util.List;
@Slf4j
@Controller
@AllArgsConstructor
public class NotificationSocketController {


    private final SimpMessagingTemplate messagingTemplate;
    private final GroupServiceImpl groupService;

    @PostMapping("/group/{groupId}")
    private void requestGroupMembership(@PathVariable("groupId")Long groupId,
                                        Principal principal){

        List<String> groupAdminAuthorities=
                groupService.usersWithGroupAdminAuthorities(groupId);

        log.info(String.valueOf(groupAdminAuthorities));

        groupAdminAuthorities.forEach(userLogin ->
                messagingTemplate.
                        convertAndSendToUser(userLogin,
                                "/queue/notifications",
                                new Notification(
                                        principal.getName(),
                                        "join group."
                                        )));
    }




}
