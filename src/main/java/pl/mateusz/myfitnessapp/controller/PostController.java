package pl.mateusz.myfitnessapp.controller;

import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import pl.mateusz.myfitnessapp.dto.comment.CommentResponse;
import pl.mateusz.myfitnessapp.dto.post.PostRequest;
import pl.mateusz.myfitnessapp.dto.post.PostResponse;
import pl.mateusz.myfitnessapp.service.PostServiceImpl;

import java.io.IOException;
import java.util.List;

@RestController
@RequestMapping("api/post")
@AllArgsConstructor
public class PostController {

    private final PostServiceImpl postService;

    @PostMapping(path = "/create", consumes = MediaType.APPLICATION_JSON_VALUE)
    @ResponseStatus(HttpStatus.CREATED)
    public void createPost(@RequestBody PostRequest postRequest) throws IOException {
        postService.createPost(postRequest);
    }

    @PutMapping(path = "/update/{id}", consumes = MediaType.APPLICATION_JSON_VALUE)
    @ResponseStatus(HttpStatus.OK)
    public void updatePost(@RequestBody PostRequest postRequest,@PathVariable("id")Long id) {
        postService.updatePost(postRequest,id);
    }

    @GetMapping( path = "/{id}",produces = MediaType.APPLICATION_JSON_VALUE)
    public PostResponse getPostById(@PathVariable Long id) {
        return postService.getPostById(id);
    }

    @GetMapping( path = "/top/{postId}/comment",produces = MediaType.APPLICATION_JSON_VALUE)
    public CommentResponse getTopCommentByPost(@PathVariable("postId")Long postId) {
        return postService.getTopCommentByPost(postId);
    }

    @GetMapping( path = "/{postId}/comments",produces = MediaType.APPLICATION_JSON_VALUE)
    public List<CommentResponse> getAllCommentsByPost(@PathVariable("postId")Long postId) {
        return postService.getAllCommentsByPost(postId);
    }
    @DeleteMapping(path = "/{id}")
    public ResponseEntity<String> deletePost(@PathVariable("id") Long id){
        postService.deletePost(id);
        return new ResponseEntity<>("Post deleted", HttpStatus.OK);
    }



}
