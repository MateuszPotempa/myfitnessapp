package pl.mateusz.myfitnessapp.controller;

import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import pl.mateusz.myfitnessapp.dto.post.PostResponse;
import pl.mateusz.myfitnessapp.dto.thread.ThreadRequest;
import pl.mateusz.myfitnessapp.dto.thread.ThreadResponse;
import pl.mateusz.myfitnessapp.service.ThreadServiceImpl;

import java.util.List;

@RestController
@RequestMapping("api/thread")
@AllArgsConstructor
public class ThreadController {

    private final ThreadServiceImpl threadService;

    @PostMapping(path = "/create", consumes = MediaType.APPLICATION_JSON_VALUE)
    @ResponseStatus(HttpStatus.CREATED)
    public void createThread(@RequestBody ThreadRequest threadDto ) {
        threadService.createThread(threadDto);
    }

    @GetMapping( path = "/{id}",produces = MediaType.APPLICATION_JSON_VALUE)
    public ThreadResponse getThreadById(@PathVariable Long id) {
        return threadService.getThreadById(id);
    }

    @GetMapping( path = "/{user}",produces = MediaType.APPLICATION_JSON_VALUE)
    public List<ThreadResponse> getAllThreadsByUser(@PathVariable String user) {
        return threadService.getAllThreadsByUser(user);
    }

    @GetMapping( path = "/{threadId}/posts",produces = MediaType.APPLICATION_JSON_VALUE)
    public List<PostResponse> getAllPostsByThread(@PathVariable("threadId")Long threadId) {
        return threadService.getAllPostsByThread(threadId);
    }

    @GetMapping(path ="/latest/{threadNumber}",produces = MediaType.APPLICATION_JSON_VALUE)
    public List<ThreadResponse> getLatestThreads(@PathVariable("threadNumber")Long threadNumber) {
        return threadService.getLatestThreads(threadNumber);
    }

    @DeleteMapping(path = "/{id}")
    public ResponseEntity<String> deleteThread(@PathVariable("id") Long id){
        threadService.deleteThread(id);
        return new ResponseEntity<>("Thread deleted", HttpStatus.OK);
    }



}
