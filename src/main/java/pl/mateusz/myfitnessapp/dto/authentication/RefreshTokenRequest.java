package pl.mateusz.myfitnessapp.dto.authentication;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotBlank;

@Data
public class RefreshTokenRequest {

    @NotBlank(message = "You cant pass empty refresh token!")
    private String refreshToken;
    private String username;
}
