package pl.mateusz.myfitnessapp.dto.authentication;

import lombok.Data;
import pl.mateusz.myfitnessapp.model.userdata.Gender;
import pl.mateusz.myfitnessapp.validation.userdata.DateFormatConstraint;
import pl.mateusz.myfitnessapp.validation.userdata.EmailExistConstraint;
import pl.mateusz.myfitnessapp.validation.userdata.LoginExistConstraint;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Data
public class RegisterRequest {


    private String firstName;

    private String lastName;

    @LoginExistConstraint(message = "User already exists!")
    @NotBlank(message = "Login is required")
    private String userLogin;

    @Size(min = 4,max = 10,message = "Password must be in range 4 to 10")
    @NotBlank
    private String password;

    @EmailExistConstraint(message = "Email already exists!")
    private String email;

    @NotNull(message = "Gender is required")
    private Gender gender;

    @DateFormatConstraint(message ="Invalid date format" )
    private String dateOfBirth;

    @NotBlank(message = "Phone number is required")
    private String phoneNumber;

}
