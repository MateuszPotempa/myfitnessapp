package pl.mateusz.myfitnessapp.dto.comment;

import lombok.Getter;
import org.springframework.lang.Nullable;

@Getter
public class CommentRequest {

    private String content;
    @Nullable
    private String imagePath;
    private Long postId;
}
