package pl.mateusz.myfitnessapp.dto.comment;


import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;

import java.time.Instant;

@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
public class CommentResponse {

    private String content;
    private String commentOwner;
    private Instant createdDate;
    private boolean isEdited;
    private String imagePath;


}
