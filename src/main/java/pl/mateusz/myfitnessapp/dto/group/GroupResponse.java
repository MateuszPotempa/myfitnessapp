package pl.mateusz.myfitnessapp.dto.group;

import lombok.Data;
import org.codehaus.jackson.annotate.JsonProperty;
import pl.mateusz.myfitnessapp.model.groupdetails.GroupType;

@Data
public class GroupResponse {


    private Long groupId;

    private String name;

    private String description;

    private GroupType groupType;

    private String groupOwner;

    private String policy;

    @JsonProperty(value = "isVisible")
    private Boolean isVisible;
}
