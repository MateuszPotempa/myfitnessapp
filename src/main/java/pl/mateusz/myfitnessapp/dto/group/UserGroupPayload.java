package pl.mateusz.myfitnessapp.dto.group;

import lombok.Data;
import pl.mateusz.myfitnessapp.model.groupdetails.GroupRole;
import pl.mateusz.myfitnessapp.validation.group.GroupRoleValidation;

@Data
public class UserGroupPayload {

    private Long userId;

    @GroupRoleValidation(message = "You cant assign OWNER role!")
    private GroupRole groupRole;


}
