package pl.mateusz.myfitnessapp.dto.post;


import lombok.Getter;
import org.springframework.lang.Nullable;

@Getter
public class PostRequest {

    private String postName;
    private String content;
    private Long threadId;
    @Nullable
    private String imageUrl;

}
