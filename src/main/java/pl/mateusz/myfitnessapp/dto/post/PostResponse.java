package pl.mateusz.myfitnessapp.dto.post;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;
import java.time.Instant;

@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
public class PostResponse {

    private String postName;
    private String content;
    private String userOwner;
    private Instant createdDate;
    private Long threadId;
    private String imageUrl;

}
