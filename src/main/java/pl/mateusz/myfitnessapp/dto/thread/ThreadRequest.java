package pl.mateusz.myfitnessapp.dto.thread;

import lombok.Data;

import javax.validation.constraints.NotNull;
import java.util.Set;

@Data
public class ThreadRequest {

    private String threadName;
    @NotNull(message = "You need to add at least one tag!")
    private Set<String> tags;
    private String description;

}
