package pl.mateusz.myfitnessapp.dto.thread;

import lombok.Data;


@Data
public class ThreadResponse {

    private String threadName;
    private String description;
    private String threadOwner;
    private Integer numberOfPosts;
    private String threadTags;
}
