package pl.mateusz.myfitnessapp.exceptions;


import lombok.Getter;
import org.springframework.http.HttpStatus;

@Getter
public enum ApiAuthErrorCode {

    LOGIN_EXIST("Login already used!",HttpStatus.BAD_REQUEST),
    EMAIL_EXIST("Email already used!",HttpStatus.BAD_REQUEST),
    VERIFICATION_TOKEN_NOT_FOUND("Verification token is not found!",HttpStatus.BAD_REQUEST),
    REFRESH_TOKEN_NOT_FOUND("Refresh token is not found!",HttpStatus.BAD_REQUEST),
    USER_NOT_FOUND("User not found",HttpStatus.NOT_FOUND),
    USER_NOT_ENABLED("Verify your account before logging",HttpStatus.BAD_REQUEST);


    final String message;
    final HttpStatus httpStatus;

    ApiAuthErrorCode(String message, HttpStatus httpStatus) {
        this.message = message;
        this.httpStatus = httpStatus;
    }

}
