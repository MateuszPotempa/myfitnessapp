package pl.mateusz.myfitnessapp.exceptions;

public class ApiCallException extends RuntimeException{

    public ApiCallException(ApiErrorCode apiErrorCode) {
        super(apiErrorCode.message);
    }
}
