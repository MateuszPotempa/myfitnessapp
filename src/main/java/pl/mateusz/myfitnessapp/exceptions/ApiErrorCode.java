package pl.mateusz.myfitnessapp.exceptions;

public enum  ApiErrorCode {

    THREAD_NOT_FOUND("Thread with given id does not exist"),
    POST_NOT_FOUND("Post with given id does not exist"),
    COMMENT_NOT_FOUND("Comment with given id does not exist"),
    GROUP_NOT_FOUND("Group with given id does not exist"),
    TAG_NOT_FOUND("Tag does not exist");



    final String message;

    ApiErrorCode(String message) {
        this.message = message;
    }
}
