package pl.mateusz.myfitnessapp.exceptions;

import lombok.Builder;
import lombok.Getter;

@Builder
@Getter
public class ApiErrorResponse {

    final String message;
    final int httpStatus;

}