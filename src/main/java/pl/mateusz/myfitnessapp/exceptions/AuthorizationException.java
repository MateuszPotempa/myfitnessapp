package pl.mateusz.myfitnessapp.exceptions;

import lombok.Getter;
import org.springframework.http.HttpStatus;

@Getter
public class AuthorizationException extends RuntimeException {

    private final String message;
    private final HttpStatus httpStatus;

    public AuthorizationException(ApiAuthErrorCode errorCode){
        this.message=errorCode.message;
        this.httpStatus=errorCode.httpStatus;

    }
}
