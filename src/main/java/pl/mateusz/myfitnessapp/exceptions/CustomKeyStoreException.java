package pl.mateusz.myfitnessapp.exceptions;

import lombok.Getter;

public class CustomKeyStoreException extends RuntimeException {


    public CustomKeyStoreException(InternalErrorCode errorCode, Exception exception){
            super(errorCode.message,exception);
    }


}
