package pl.mateusz.myfitnessapp.exceptions;


import lombok.Getter;
import org.springframework.http.HttpStatus;

@Getter
public enum InternalErrorCode {

    KEY_STORE("Exception occurred while loading keystore"),
    PRIVATE_KEY("Exception occurred while retrieving private key from keystore"),
    PUBLIC_KEY("Exception occured while retrieving public key from keystore");


    final String message;

    InternalErrorCode(String message) {
        this.message = message;
    }

}
