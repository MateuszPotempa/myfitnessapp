package pl.mateusz.myfitnessapp.mapper;

import org.mapstruct.*;
import pl.mateusz.myfitnessapp.dto.comment.CommentRequest;
import pl.mateusz.myfitnessapp.dto.comment.CommentResponse;
import pl.mateusz.myfitnessapp.model.Comment;
import pl.mateusz.myfitnessapp.model.Post;
import pl.mateusz.myfitnessapp.model.userdata.User;

@Mapper(componentModel = "spring")
public interface CommentMapper {

    @Mapping(target ="content",source = "commentRequest.content")
    @Mapping(target ="imagePath",ignore = true)
    @Mapping(target ="id",ignore = true)
    @Mapping(target ="post",source = "post")
    @Mapping(target ="user",source = "user")
    @Mapping(target ="creationDate",expression = "java(java.time.Instant.now())")
    Comment map(CommentRequest commentRequest, Post post, User user);

    @Mapping(target ="content",source = "comment.content")
    @Mapping(target ="createdDate",source ="comment.creationDate")
    @Mapping(target ="commentOwner",source = "comment.user.userLogin")
    @Mapping(target = "edited",expression = "java(checkEdition(comment))")
    CommentResponse mapToDto(Comment comment);


    @BeanMapping(nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE)
    @Mapping(target = "editionDate", expression = "java(java.time.Instant.now())")
    void updateComment(CommentRequest commentRequest, @MappingTarget Comment comment);

    default Boolean checkEdition(Comment comment){
        return comment.getEditionDate()!=null;
    }
}
