package pl.mateusz.myfitnessapp.mapper;


import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.springframework.beans.factory.annotation.Autowired;
import pl.mateusz.myfitnessapp.dto.group.GroupDto;
import pl.mateusz.myfitnessapp.dto.group.GroupResponse;
import pl.mateusz.myfitnessapp.model.groupdetails.Group;
import pl.mateusz.myfitnessapp.model.groupdetails.GroupRole;

import pl.mateusz.myfitnessapp.repository.UserGroupRepository;

@Mapper(componentModel = "spring")
public abstract class GroupMapper {

    @Autowired
    private UserGroupRepository userGroupRepository;


    @Mapping(target ="info.description",source = "groupDto.description")
    @Mapping(target ="info.groupType",source ="groupDto.groupType")
    @Mapping(target ="info.isVisible",source = "groupDto.isVisible")
    @Mapping(target ="info.policy",source = "groupDto.policy")
    @Mapping(target ="name",source = "groupDto.name")
    public abstract Group map(GroupDto groupDto);

    @Mapping(target ="name",source = "group.name")
    @Mapping(target ="policy",source = "group.info.policy")
    @Mapping(target ="groupType",source = "group.info.groupType")
    @Mapping(target ="description",source = "group.info.description")
    @Mapping(target ="groupId",source = "group.id")
    @Mapping(target ="groupOwner",expression = "java(getOwnerName(group))")
    @Mapping(target = "isVisible", source = "group.info.isVisible")
    public abstract GroupResponse mapToDto(Group group);

    String getOwnerName(Group group){

       return userGroupRepository
               .findByGroupRoleAndGroups(GroupRole.GROUP_OWNER,group)
               .getUser()
               .getUserLogin();
    }


}
