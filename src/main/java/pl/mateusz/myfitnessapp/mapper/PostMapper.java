package pl.mateusz.myfitnessapp.mapper;

import org.mapstruct.*;
import pl.mateusz.myfitnessapp.dto.post.PostRequest;
import pl.mateusz.myfitnessapp.dto.post.PostResponse;
import pl.mateusz.myfitnessapp.model.Post;
import pl.mateusz.myfitnessapp.model.Thread;
import pl.mateusz.myfitnessapp.model.userdata.User;

@Mapper(componentModel = "spring")
public interface PostMapper {

    @Mapping(target ="postName",source = "postRequest.postName")
    @Mapping(target ="imagePath",ignore = true)
    @Mapping(target ="id",ignore = true)
    @Mapping(target = "createdDate", expression = "java(java.time.Instant.now())")
    @Mapping(target = "content",source = "postRequest.content")
    @Mapping(target = "thread", source = "thread")
    @Mapping(target = "user", source = "user")
    Post map(PostRequest postRequest,Thread thread, User user);


    @Mapping(target ="content",source = "post.content")
    @Mapping(target ="postName",source = "post.postName")
    @Mapping(target ="createdDate",source = "post.createdDate")
    @Mapping(target ="threadId",source = "post.thread.id")
    @Mapping(target ="userOwner",source = "post.user.userLogin")
    @Mapping(target ="imageUrl",source = "post.imagePath")
    PostResponse mapToDto(Post post);

    @BeanMapping(nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE)
    void updatePost(PostRequest postRequest, @MappingTarget Post post);
}
