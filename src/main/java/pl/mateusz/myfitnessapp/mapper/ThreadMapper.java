package pl.mateusz.myfitnessapp.mapper;


import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import pl.mateusz.myfitnessapp.dto.thread.ThreadRequest;
import pl.mateusz.myfitnessapp.dto.thread.ThreadResponse;
import pl.mateusz.myfitnessapp.model.Post;
import pl.mateusz.myfitnessapp.model.Thread;
import pl.mateusz.myfitnessapp.model.ThreadTag;
import pl.mateusz.myfitnessapp.model.userdata.User;

import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;


@Mapper(componentModel = "spring")
public interface ThreadMapper {


    @Mapping(target ="threadName",source = "threadDto.threadName")
    @Mapping(target ="description",source = "threadDto.description")
    @Mapping(target = "createdDate", expression = "java(java.time.Instant.now())")
    @Mapping(target = "tags", ignore = true)
    @Mapping(target = "id", ignore = true)
    @Mapping(target = "user", source = "user")
    Thread map(ThreadRequest threadDto, User user);


    @Mapping(target = "description",source = "thread.description")
    @Mapping(target = "threadName",source = "thread.threadName")
    @Mapping(target = "threadOwner",source = "thread.user.userLogin")
    @Mapping(target = "numberOfPosts",expression ="java(mapPosts(thread.getPosts()))")
    @Mapping(target = "threadTags",expression ="java(mapTags(thread.getTags()))")
    ThreadResponse mapToDto(Thread thread);


    default Integer mapPosts(List<Post> numberOfPosts) {
        return numberOfPosts.size();
    }

    default String mapTags(Set<ThreadTag> threadTags){

        return threadTags
                .stream()
                .map(ThreadTag::getTag)
                .collect(Collectors.joining(","));
    }






}
