package pl.mateusz.myfitnessapp.mapper;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import pl.mateusz.myfitnessapp.model.groupdetails.Group;
import pl.mateusz.myfitnessapp.model.groupdetails.GroupRole;
import pl.mateusz.myfitnessapp.model.userdata.User;
import pl.mateusz.myfitnessapp.model.userdata.UserGroup;

@Mapper(componentModel = "spring")
public interface UserGroupMapper {


    @Mapping(target = "user", source = "user")
    @Mapping(target = "groupRole", source = "groupRole")
    @Mapping(target = "groups", ignore = true)
    @Mapping(target = "id", ignore = true)
    UserGroup map(User user, GroupRole groupRole);

}