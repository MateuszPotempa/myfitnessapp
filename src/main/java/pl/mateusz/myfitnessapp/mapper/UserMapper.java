package pl.mateusz.myfitnessapp.mapper;


import org.mapstruct.*;
import org.springframework.security.oauth2.provider.OAuth2Request;
import pl.mateusz.myfitnessapp.dto.authentication.RegisterRequest;
import pl.mateusz.myfitnessapp.model.token.AuthProvider;
import pl.mateusz.myfitnessapp.model.userdata.User;
import pl.mateusz.myfitnessapp.security.oauth2.user.OAuth2UserInfo;

@Mapper(componentModel = "spring",unmappedTargetPolicy = ReportingPolicy.IGNORE)
public interface UserMapper {


    @Mapping(target = "personalInformation.firstName", source = "firstName")
    @Mapping(target = "personalInformation.gender", source = "gender")
    @Mapping(target = "personalInformation.lastName", source = "lastName")
    @Mapping(target = "personalInformation.dateOfBirth", source = "dateOfBirth")
    @Mapping(target = "personalInformation.phoneNumber", source = "phoneNumber")
    @Mapping(target = "enabled", constant = "false")
    @Mapping(target = "userLogin", source = "userLogin")
    @Mapping(target = "email", source = "email")
    @Mapping(target = "password", ignore = true)
    @Mapping(target = "imageUrl", ignore = true)
    @Mapping(target = "providerId", ignore = true)
    @Mapping(target = "authProvider",constant = "local")
    @Mapping(target = "dateOfCreation", expression = "java(java.sql.Timestamp.from(java.time.Instant.now()))")
    User map(RegisterRequest registerRequest);



    @Mapping(target = "personalInformation.firstName", source = "oAuth2UserInfo.firstName")
    @Mapping(target = "personalInformation.lastName", source = "oAuth2UserInfo.lastName")
    @Mapping(target = "enabled", constant = "true")
    @Mapping(target = "email", source = "oAuth2UserInfo.email")
    @Mapping(target = "password", ignore = true)
    @Mapping(target = "imageUrl",source = "oAuth2UserInfo.imageUrl" )
    @Mapping(target = "providerId", source = "oAuth2UserInfo.id")
    @Mapping(target = "authProvider",source = "authProvider")
    @Mapping(target = "id",ignore = true)
    @Mapping(target = "dateOfCreation", expression = "java(java.sql.Timestamp.from(java.time.Instant.now()))")
    User oAuth2MapToDao(AuthProvider authProvider, OAuth2UserInfo oAuth2UserInfo);


}
