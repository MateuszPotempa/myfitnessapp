package pl.mateusz.myfitnessapp.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.lang.Nullable;
import pl.mateusz.myfitnessapp.model.userdata.User;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import java.time.Instant;

@Data
@Entity
@NoArgsConstructor
@AllArgsConstructor
public class Comment {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotBlank(message = "Comment must not be blank!")
    private String content;

    @Nullable
    private String imagePath;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name ="userId", referencedColumnName = "id")
    private User user;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name ="postId", referencedColumnName = "id")
    private Post post;

    private Instant creationDate;

    private Instant editionDate;
}
