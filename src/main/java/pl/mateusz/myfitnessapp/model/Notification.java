package pl.mateusz.myfitnessapp.model;

public class Notification {

    private String senderUserName;
    private String message;


    public Notification(String senderUserName, String message) {
        this.senderUserName = senderUserName;
        this.message = message;
    }

    public Notification() {
    }

    public String getSenderUserName() {
        return senderUserName;
    }

    public void setSenderUserName(String senderUserName) {
        this.senderUserName = senderUserName;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
