package pl.mateusz.myfitnessapp.model;


import lombok.*;
import org.springframework.lang.Nullable;
import pl.mateusz.myfitnessapp.model.userdata.User;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import java.time.Instant;
import java.util.List;

@Data
@Entity
@NoArgsConstructor
@AllArgsConstructor
public class Post {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotBlank(message = "Post name can't be empty")
    private String postName;

    @Nullable
    @Getter(AccessLevel.NONE)
    private String imagePath;

    @Nullable
    @Lob
    private String content;

    private Instant createdDate;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name ="userId",referencedColumnName ="id" )
    private User user;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name ="threadId",referencedColumnName ="id" )
    private Thread thread;

    @OneToMany(mappedBy ="post",
            fetch=FetchType.LAZY,
            cascade = CascadeType.REMOVE)
    private List<Comment> comments;


    @Nullable
    public String getImagePath() {
        return "post/user "+user.getId()+"/post "+id+"/"+imagePath;
    }


}
