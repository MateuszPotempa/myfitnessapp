package pl.mateusz.myfitnessapp.model;


import lombok.Data;

import javax.persistence.*;

@Entity
@Data
@Table(name = "tag_count")
public class TagCount {

    @Id
    private String tag;

    private Integer tagCount;

}
