package pl.mateusz.myfitnessapp.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import pl.mateusz.myfitnessapp.model.userdata.User;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import java.time.Instant;
import java.util.List;
import java.util.Set;

@Data
@Entity
@EqualsAndHashCode(exclude="tags")
public class Thread {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotBlank(message = "Thread name must not be empty!")
    private String threadName;

    @NotBlank(message = "Thread name must not be empty!")
    private String description;

    @OneToMany(mappedBy = "thread",fetch=FetchType.LAZY,cascade = {CascadeType.PERSIST,CascadeType.REMOVE})
    private Set<ThreadTag> tags;

    private Instant createdDate;

    @OneToMany(mappedBy ="thread", fetch=FetchType.LAZY, cascade =  {CascadeType.PERSIST,CascadeType.REMOVE})
    private List<Post> posts;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name ="userId",referencedColumnName ="id" )
    private User user;
}
