package pl.mateusz.myfitnessapp.model;


import lombok.Data;
import lombok.EqualsAndHashCode;
import pl.mateusz.myfitnessapp.model.userdata.User;


import javax.persistence.*;


@Entity
@Data

@Table(name = "thread_tag")
public class ThreadTag {

        @Id
        @GeneratedValue(strategy = GenerationType.IDENTITY)
        private Long id;

        private String tag;

        //tutaj trzeba dac ze mozna dodac ale nie mozna usuwac jak usunie sie jednego taga
        @ManyToOne(cascade = CascadeType.ALL)
        private Thread thread;

        @ManyToOne
        @JoinColumn(name = "user_id")
        private User user;


}
