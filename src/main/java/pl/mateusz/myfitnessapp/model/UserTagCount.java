package pl.mateusz.myfitnessapp.model;

import lombok.Data;
import pl.mateusz.myfitnessapp.model.userdata.User;

import javax.persistence.*;

@Entity
@Data
@Table(name = "user_tag_count")
public class UserTagCount {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    
    private String tag;

    @ManyToOne
    @JoinColumn(name = "user_id")
    private User user;

    private Integer userTagCount;

}
