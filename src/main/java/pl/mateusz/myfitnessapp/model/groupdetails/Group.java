package pl.mateusz.myfitnessapp.model.groupdetails;

import lombok.Data;
import pl.mateusz.myfitnessapp.model.Thread;
import pl.mateusz.myfitnessapp.model.userdata.UserGroup;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.List;

@Data
@Entity
@Table(name = "_group")
public class Group {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotNull(message = "Name of the group is required")
    private String name;

    @Embedded
    @AttributeOverrides({
            @AttributeOverride( name = "groupType", column = @Column(name = "group_type")),
            @AttributeOverride( name = "description", column = @Column(name = "group_description")),
            @AttributeOverride( name = "policy", column = @Column(name = "group_policy")),
            @AttributeOverride( name = "isVisible", column = @Column(name = "group_visible"))})
    private GroupInfo info;

    @ManyToMany(cascade = {
        CascadeType.PERSIST,
            CascadeType.MERGE})
    @JoinTable(
            name = "user_groups",
            joinColumns = { @JoinColumn(name = "group_id") },
            inverseJoinColumns = { @JoinColumn(name = "user_id") }
    )
    private List<UserGroup> userGroup=new ArrayList<>();

    @OneToMany
    private List<Thread> thread;


    public void addUserGroup(UserGroup user) {
        userGroup.add(user);
        user.getGroups().add(this);
    }

    public void removeUserGroup(UserGroup user) {
        userGroup.remove(user);
        user.getGroups().remove(this);
    }



}
