package pl.mateusz.myfitnessapp.model.groupdetails;

import lombok.Data;

import javax.persistence.Embeddable;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@Embeddable
@Data
public class GroupInfo {


    @Enumerated(EnumType.STRING)
    @NotNull(message = "GroupType has to be set!")
    private GroupType groupType;

    @NotBlank(message = "Description must not be blank!")
    private String description;

    @NotBlank(message = "Please choose group policy!")
    private String policy;

    @NotBlank(message = "Please choose visibility option!")
    private Boolean isVisible;


}

