package pl.mateusz.myfitnessapp.model.groupdetails;

import com.fasterxml.jackson.annotation.JsonCreator;
import org.codehaus.jackson.annotate.JsonValue;
import org.springframework.security.core.GrantedAuthority;

import java.util.stream.Stream;

public enum GroupRole {

    GROUP_OWNER("OWNER"),
    GROUP_ADMIN("ADMIN"),
    GROUP_EDITOR("EDITOR"),
    GROUP_USER("USER");

    private final String role;

    GroupRole(String role) {
        this.role = role;
    }


    @JsonCreator
    public static GroupRole decode(final String code) {
        return Stream.of(GroupRole.values())
                .filter(groupRole -> groupRole.role.equals(code))
                .findFirst()
                .orElse(null);
    }

    @JsonValue
    public String getRole() {
        return role;
    }
}
