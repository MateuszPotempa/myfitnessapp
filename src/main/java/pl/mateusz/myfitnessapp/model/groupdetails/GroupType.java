package pl.mateusz.myfitnessapp.model.groupdetails;

import com.fasterxml.jackson.annotation.JsonCreator;
import org.codehaus.jackson.annotate.JsonValue;

import java.util.stream.Stream;

public enum GroupType {

    GROUP_PUBLIC("PUBLIC"),
    GROUP_PRIVATE("PRIVATE"),
    GROUP_SECRET("SECRET");

    private final String type;

    GroupType(String type) {
        this.type = type;
    }

    @JsonCreator
    public static GroupType decode(final String code) {
        return Stream.of(GroupType.values())
                .filter(groupType -> groupType.type.equals(code))
                .findFirst()
                .orElse(null);
    }

    @JsonValue
    public String getType() {
        return type;
    }
}
