package pl.mateusz.myfitnessapp.model.token;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.Id;
import java.util.Date;

@Data
@Entity
@NoArgsConstructor
public class RefreshToken {

    @Id
    private String userName;
    private String refreshToken;
    private Date expirationTime;



}
