package pl.mateusz.myfitnessapp.model.userdata;

import com.fasterxml.jackson.annotation.JsonCreator;
import org.codehaus.jackson.annotate.JsonValue;
import pl.mateusz.myfitnessapp.model.groupdetails.GroupRole;

import java.util.stream.Stream;

public enum Gender {
    MALE("Male"),
    FEMALE("Female");

    private final String genderType;

    Gender(String genderType) {
        this.genderType = genderType;
    }

    @JsonCreator
    public static Gender decode(final String code) {
        return Stream.of(Gender.values())
                .filter(gender -> gender.genderType.equals(code))
                .findFirst()
                .orElse(null);
    }

    @JsonValue
    public String getGenderType() {
        return genderType;
    }
}
