package pl.mateusz.myfitnessapp.model.userdata;

import lombok.*;
import org.springframework.lang.Nullable;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.validation.annotation.Validated;
import pl.mateusz.myfitnessapp.model.token.AuthProvider;

import javax.persistence.*;
import javax.validation.constraints.*;
import java.sql.Timestamp;
import java.util.Collection;
import java.util.Collections;

@Entity
@NoArgsConstructor
@AllArgsConstructor
@Data
public class User {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String userLogin;

    private String password;

    @Email(message = "Bad Email Format")
    @NotEmpty(message = "Email is required")
    private String email;

    @OneToOne(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    private PersonalInformation personalInformation;

    @Nullable
    private String imageUrl;

    @NotNull
    @Enumerated(EnumType.STRING)
    private AuthProvider authProvider;

    @Nullable
    private String providerId;

    private Boolean enabled;

    @Column(name = "date_created")
    private Timestamp dateOfCreation;




}
