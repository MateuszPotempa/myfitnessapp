package pl.mateusz.myfitnessapp.model.userdata;

import lombok.Data;
import pl.mateusz.myfitnessapp.model.groupdetails.Group;
import pl.mateusz.myfitnessapp.model.groupdetails.GroupRole;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.List;

@Entity
@Data
public class UserGroup {

    @Id
    private Long id;

    @OneToOne(fetch = FetchType.LAZY)
    @MapsId
    private User user;

    @Enumerated(EnumType.STRING)
    @NotNull(message = "User role in group is required")
    private GroupRole groupRole;

    @ManyToMany(mappedBy = "userGroup")
    private List<Group> groups=new ArrayList<>();

}
