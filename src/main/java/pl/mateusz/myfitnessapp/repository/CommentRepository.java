package pl.mateusz.myfitnessapp.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import pl.mateusz.myfitnessapp.model.Comment;

import java.util.List;
import java.util.Optional;

@Repository
public interface CommentRepository extends JpaRepository<Comment,Long> {

    List<Comment> findAllByPostId(Long postId);

    Optional<Comment> findTopByPostIdOrderByIdDesc(Long postId);

    @Query("SELECT max(c.id) FROM Comment c")
    Optional<Long> findFirstByOrderByIdDesc();

}
