package pl.mateusz.myfitnessapp.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import pl.mateusz.myfitnessapp.model.groupdetails.Group;
import pl.mateusz.myfitnessapp.model.groupdetails.GroupRole;
import pl.mateusz.myfitnessapp.model.userdata.UserGroup;

@Repository
public interface GroupRepository extends JpaRepository<Group,Long> {


}
