package pl.mateusz.myfitnessapp.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import pl.mateusz.myfitnessapp.model.Comment;
import pl.mateusz.myfitnessapp.model.Post;

import java.util.List;
import java.util.Optional;

@Repository
public interface PostRepository extends JpaRepository<Post,Long> {


    List<Post>findAllByThreadId(Long threadId);

    @Query("SELECT max(p.id) FROM Post p")
    Optional<Long> findFirstByOrderByIdDesc();

}
