package pl.mateusz.myfitnessapp.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import pl.mateusz.myfitnessapp.model.TagCount;

import java.util.Optional;

@Repository
public interface TagCountRepository extends JpaRepository<TagCount,String> {

    TagCount findByTag(String tag);
}
