package pl.mateusz.myfitnessapp.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import pl.mateusz.myfitnessapp.model.Post;
import pl.mateusz.myfitnessapp.model.Thread;

import java.util.List;

@Repository
public interface ThreadRepository extends JpaRepository<Thread,Long> {

        List<Thread> findAllByUser_UserLogin(String userLogin);

        List<Thread>findAllByOrderByIdDesc();

}
