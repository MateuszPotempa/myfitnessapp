package pl.mateusz.myfitnessapp.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import pl.mateusz.myfitnessapp.model.ThreadTag;



@Repository
public interface ThreadTagRepository extends JpaRepository<ThreadTag,String> {



}
