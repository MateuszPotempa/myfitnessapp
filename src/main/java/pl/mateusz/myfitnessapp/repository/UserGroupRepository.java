package pl.mateusz.myfitnessapp.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import pl.mateusz.myfitnessapp.model.groupdetails.Group;
import pl.mateusz.myfitnessapp.model.groupdetails.GroupRole;
import pl.mateusz.myfitnessapp.model.userdata.User;
import pl.mateusz.myfitnessapp.model.userdata.UserGroup;

import java.util.List;
import java.util.Optional;

public interface UserGroupRepository extends JpaRepository<UserGroup,Long> {

    UserGroup findByGroupRoleAndGroups(GroupRole groupRole,Group group);

    List<UserGroup> findAllByGroupRoleInAndGroups(List<GroupRole>groupRoles,Group group);

    UserGroup findByGroupRoleInAndGroupsAndUser(List<GroupRole> groupRole, Group groupId, User user);

    UserGroup findByUserAndGroups(User user, Group group);

}
