package pl.mateusz.myfitnessapp.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import pl.mateusz.myfitnessapp.model.userdata.User;

import java.util.Optional;

@Repository
public interface UserRepository extends JpaRepository<User,Long> {

    Optional<User>findByUserLogin(String  userName);

    @Query("select t from User t where  t.userLogin = ?1 or t.email = ?1 ")
    Optional<User>findByUserLoginOrEmail(String userLoginOrEmail);

    @Query("select t from User t where  (t.userLogin = ?1 or t.email = ?1) and t.enabled=true")
    Optional<User> checkIfUserEnabled(String userLoginOrEmail);

    Boolean existsByEmail(String email);
    Boolean existsByUserLogin(String userLogin);

}
