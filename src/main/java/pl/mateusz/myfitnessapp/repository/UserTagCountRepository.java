package pl.mateusz.myfitnessapp.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import pl.mateusz.myfitnessapp.model.UserTagCount;
import pl.mateusz.myfitnessapp.model.userdata.User;

import java.util.Optional;

@Repository
public interface UserTagCountRepository extends JpaRepository<UserTagCount,Long> {

    Optional<UserTagCount> findByTagAndUser(String tag, User user);
}
