package pl.mateusz.myfitnessapp.security;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;
import pl.mateusz.myfitnessapp.exceptions.ApiAuthErrorCode;
import pl.mateusz.myfitnessapp.exceptions.ApiCallException;
import pl.mateusz.myfitnessapp.exceptions.ApiErrorCode;
import pl.mateusz.myfitnessapp.exceptions.AuthorizationException;
import pl.mateusz.myfitnessapp.model.groupdetails.Group;
import pl.mateusz.myfitnessapp.model.groupdetails.GroupRole;
import pl.mateusz.myfitnessapp.model.userdata.User;
import pl.mateusz.myfitnessapp.model.userdata.UserGroup;
import pl.mateusz.myfitnessapp.repository.GroupRepository;
import pl.mateusz.myfitnessapp.repository.UserGroupRepository;
import pl.mateusz.myfitnessapp.repository.UserRepository;

import java.util.Arrays;
import java.util.List;

@Component
@AllArgsConstructor
@Slf4j
public class GroupPermissionEvaluator {

    private final UserGroupRepository userGroupRepository;
    private final GroupRepository groupRepository;
    private final UserRepository userRepository;


    public boolean hasGroupAdminRole(String currentUser,Long groupId) {

        UserGroup userGroup=userGroupRoleVerification(currentUser,
                groupId,
                Arrays.asList(GroupRole.GROUP_ADMIN,GroupRole.GROUP_OWNER));

        return userGroup!=null;

    }


    private UserGroup userGroupRoleVerification(String currentUser, Long groupId, List<GroupRole> groupRole){

        User currentUserDao=userRepository
                .findByUserLogin(currentUser)
                .orElseThrow(()->new AuthorizationException(ApiAuthErrorCode.USER_NOT_FOUND));

        Group group= groupRepository
                .findById(groupId)
                .orElseThrow(()->new ApiCallException(ApiErrorCode.GROUP_NOT_FOUND));

        return userGroupRepository
                .findByGroupRoleInAndGroupsAndUser(groupRole,
                        group,currentUserDao);
    }

}
