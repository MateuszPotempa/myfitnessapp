package pl.mateusz.myfitnessapp.security.JWT;

import io.jsonwebtoken.Jwts;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import pl.mateusz.myfitnessapp.exceptions.ApiAuthErrorCode;
import pl.mateusz.myfitnessapp.exceptions.AuthorizationException;
import pl.mateusz.myfitnessapp.exceptions.CustomKeyStoreException;
import pl.mateusz.myfitnessapp.exceptions.InternalErrorCode;
import pl.mateusz.myfitnessapp.model.token.RefreshToken;
import pl.mateusz.myfitnessapp.repository.RefreshTokenRepository;
import pl.mateusz.myfitnessapp.service.UserDetailsServiceImpl;

import javax.annotation.PostConstruct;
import java.io.IOException;
import java.io.InputStream;
import java.security.*;
import java.security.cert.CertificateException;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.Date;

@Service
public class JwtProvider {

    private KeyStore keyStore;

    @Value("${jwt.keystore.password}")
    private String keyStorePassword;

    private final RefreshTokenRepository refreshTokenRepository;
    private final PasswordEncoder tokenEncoder;
    private final UserDetailsServiceImpl userDetailsService;


    public JwtProvider(RefreshTokenRepository refreshTokenRepository, @Lazy PasswordEncoder tokenEncoder, UserDetailsServiceImpl userDetailsService) {
        this.refreshTokenRepository = refreshTokenRepository;
        this.tokenEncoder = tokenEncoder;
        this.userDetailsService = userDetailsService;
    }

    @PostConstruct
    public void init() {
        try {
            keyStore = KeyStore.getInstance("JKS");
            InputStream resourceAsStream = getClass().getResourceAsStream("/myfitnessapp.jks");
            keyStore.load(resourceAsStream, keyStorePassword.toCharArray());
        } catch (KeyStoreException | IOException | NoSuchAlgorithmException | CertificateException e) {
            throw new CustomKeyStoreException(InternalErrorCode.KEY_STORE, e);
        }
    }

    private PublicKey getPublicKey() {
        try {
            return keyStore.getCertificate("myfitnessapp").getPublicKey();
        } catch (KeyStoreException e) {
            throw new CustomKeyStoreException(InternalErrorCode.PUBLIC_KEY, e);
        }
    }

    private PrivateKey getPrivateKey() {
        try {
            return (PrivateKey) keyStore.getKey("myfitnessapp", keyStorePassword.toCharArray());
        } catch (KeyStoreException | NoSuchAlgorithmException | UnrecoverableKeyException e) {
            throw new CustomKeyStoreException(InternalErrorCode.PRIVATE_KEY, e);
        }
    }


    public String generateToken(Authentication authentication) {

        String username = authentication.getName();

        return Jwts.builder()
                .setSubject(username)
                .setIssuedAt(Date.from(Instant.now()))
                .signWith(getPrivateKey())
                .setExpiration(Date.from(Instant.now().plus(1000000, ChronoUnit.SECONDS)))
                .compact();
    }

    public String generateTokenWithUserName(String username) {

        return Jwts.builder()
                .setSubject(username)
                .setIssuedAt(Date.from(Instant.now()))
                .signWith(getPrivateKey())
                .setExpiration(Date.from(Instant.now().plus(100000000, ChronoUnit.SECONDS)))
                .compact();
    }

    public String generateRefreshToken(String username) {

        String refreshToken = Jwts.builder()
                .setSubject(username)
                .setIssuedAt(Date.from(Instant.now()))
                .signWith(getPrivateKey())
                .setExpiration(Date.from(Instant.now().plus(1, ChronoUnit.DAYS)))
                .compact();

        RefreshToken userRefreshToken = new RefreshToken();
        userRefreshToken.setRefreshToken(tokenEncoder.encode(refreshToken));
        userRefreshToken.setUserName(username);
        userRefreshToken.setExpirationTime(getExpirationDateFromRefreshToken(refreshToken));
        refreshTokenRepository.save(userRefreshToken);

        return refreshToken;
    }


    public boolean validateToken(String userToken) {
        Jwts.parser().setSigningKey(getPublicKey()).parseClaimsJws(userToken);

        return true;
    }

    public boolean validateRefreshToken(String userName) {
        refreshTokenRepository.existsByUserName(userName)
                .orElseThrow(() -> new AuthorizationException(ApiAuthErrorCode.REFRESH_TOKEN_NOT_FOUND));

        return true;
    }

    public String getUserNameFromJwt(String userToken) {
        return Jwts.parser()
                .setSigningKey(getPublicKey())
                .parseClaimsJws(userToken)
                .getBody()
                .getSubject();
    }

    public Date getExpirationDateFromRefreshToken(String token) {
        return Jwts.parser()
                .setSigningKey(getPublicKey())
                .parseClaimsJws(token)
                .getBody().getExpiration();
    }


    public Authentication getAuthenticationFromToken(String token) {

        if (token != null) {

            String userName = getUserNameFromJwt(token);
            UserDetails user = userDetailsService.loadUserByUsername(userName);

            if (user != null)
                return new UsernamePasswordAuthenticationToken(user, null, user.getAuthorities());
        }

        return null;
    }
}