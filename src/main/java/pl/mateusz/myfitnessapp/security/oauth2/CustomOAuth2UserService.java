package pl.mateusz.myfitnessapp.security.oauth2;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.authentication.InternalAuthenticationServiceException;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.oauth2.client.OAuth2AuthorizedClient;
import org.springframework.security.oauth2.client.OAuth2AuthorizedClientService;
import org.springframework.security.oauth2.client.userinfo.DefaultOAuth2UserService;
import org.springframework.security.oauth2.client.userinfo.OAuth2UserRequest;
import org.springframework.security.oauth2.core.OAuth2AuthenticationException;
import org.springframework.security.oauth2.core.oidc.OidcIdToken;
import org.springframework.security.oauth2.core.oidc.OidcUserInfo;
import org.springframework.security.oauth2.core.user.OAuth2User;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import pl.mateusz.myfitnessapp.exceptions.oauth2.OAuth2AuthenticationProcessingException;
import pl.mateusz.myfitnessapp.mapper.UserMapper;
import pl.mateusz.myfitnessapp.model.token.AuthProvider;
import pl.mateusz.myfitnessapp.model.userdata.User;
import pl.mateusz.myfitnessapp.repository.UserRepository;
import pl.mateusz.myfitnessapp.security.oauth2.user.OAuth2UserInfo;
import pl.mateusz.myfitnessapp.security.oauth2.user.OAuth2UserInfoFactory;
import pl.mateusz.myfitnessapp.service.UserPrincipal;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

@AllArgsConstructor
@Service
@Slf4j
public class CustomOAuth2UserService extends DefaultOAuth2UserService {
    
    private final UserRepository userRepository;
    private final UserMapper userMapper;


    @Override
    public OAuth2User loadUser(OAuth2UserRequest oAuth2UserRequest) throws OAuth2AuthenticationException {
        OAuth2User oAuth2User= super.loadUser(oAuth2UserRequest);
        log.info(oAuth2User.toString());
        try {
            Map<String, Object> attributes = new HashMap<>(oAuth2User.getAttributes());
            return processOAuth2User(oAuth2UserRequest.getClientRegistration().getRegistrationId(), attributes,null,null);
        } catch (AuthenticationException ex) {
            throw ex;
        } catch (Exception ex) {
            throw new InternalAuthenticationServiceException(ex.getMessage(), ex.getCause());
        }
    }

    public UserPrincipal processOAuth2User(String registrationId, Map<String, Object> attributes, OidcIdToken idToken, OidcUserInfo userInfo) {
        OAuth2UserInfo oAuth2UserInfo = OAuth2UserInfoFactory
                .getOAuth2UserInfo(registrationId,
                        attributes);

        if(StringUtils.isEmpty(oAuth2UserInfo.getEmail())) {
            throw new OAuth2AuthenticationProcessingException("Email not found from OAuth2 provider");
        }

        AuthProvider authProvider= AuthProvider.valueOf(registrationId);
        Optional<User> userOptional = userRepository.findByUserLoginOrEmail(oAuth2UserInfo.getEmail());
        User user;
        if(userOptional.isPresent()) {
            user = userOptional.get();
            if(!user.getAuthProvider().equals(authProvider)) {
                throw new OAuth2AuthenticationProcessingException("Looks like you're signed up with " +
                        user.getAuthProvider() + " account. Please use your " + user.getAuthProvider() +
                        " account to login.");
            }
            user = updateExistingUser(user, oAuth2UserInfo,authProvider);
        } else {
            user = registerNewUser(oAuth2UserInfo,authProvider);
        }

        return UserPrincipal.create(user, attributes,idToken,userInfo);
    }

    private User registerNewUser(OAuth2UserInfo oAuth2UserInfo,AuthProvider authProvider) {
        User newOAuth2User=userMapper.oAuth2MapToDao(authProvider,oAuth2UserInfo);
        log.info(newOAuth2User.toString());
        return userRepository.save(newOAuth2User);
    }

    private User updateExistingUser(User existingUser, OAuth2UserInfo oAuth2UserInfo,AuthProvider authProvider) {
        existingUser.setImageUrl(oAuth2UserInfo.getImageUrl());
        existingUser.setAuthProvider(authProvider);
      return userRepository.save(existingUser);
    }
}
