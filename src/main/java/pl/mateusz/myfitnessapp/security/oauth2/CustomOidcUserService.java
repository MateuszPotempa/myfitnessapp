package pl.mateusz.myfitnessapp.security.oauth2;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.oauth2.client.oidc.userinfo.OidcUserRequest;
import org.springframework.security.oauth2.client.oidc.userinfo.OidcUserService;
import org.springframework.security.oauth2.core.OAuth2AuthenticationException;
import org.springframework.security.oauth2.core.oidc.user.OidcUser;
import org.springframework.stereotype.Service;
import pl.mateusz.myfitnessapp.exceptions.oauth2.OAuth2AuthenticationProcessingException;

import javax.naming.AuthenticationException;

@Service
@AllArgsConstructor
@Slf4j
public class CustomOidcUserService extends OidcUserService {

    private final CustomOAuth2UserService customOAuth2UserService;


    @Override
    public OidcUser loadUser(OidcUserRequest userRequest) throws OAuth2AuthenticationException {
        OidcUser oidcUser = super.loadUser(userRequest);
        log.info(oidcUser.toString());
        log.info(userRequest.getIdToken().toString());
        try {
            return customOAuth2UserService.processOAuth2User(userRequest.getClientRegistration().getRegistrationId(), oidcUser.getAttributes(), oidcUser.getIdToken(),
                    oidcUser.getUserInfo());
        } catch (Exception ex) {
            ex.printStackTrace();

            throw new OAuth2AuthenticationProcessingException(ex.getMessage(), ex.getCause());
        }    }
}
