package pl.mateusz.myfitnessapp.security.oauth2.user;

import java.util.Map;

public class GitHubOAuth2UserInfo  extends OAuth2UserInfo{


    public GitHubOAuth2UserInfo(Map<String, Object> attributes) {
        super(attributes);
    }

    @Override
    public String getId() {
        return ((Integer) attributes.get("id")).toString();
    }

    @Override
    public String getFirstName() {
        return getName()[0];
    }

    @Override
    public String getLastName() {
        return getName()[1];
    }

    @Override
    public String getEmail() {
        return (String) attributes.get("email");
    }

    @Override
    public String getImageUrl() {
        return (String) attributes.get("avatar_url");
    }

    private String [] getName(){
        String fullName=(String)attributes.get("name");
        return fullName.split(" ");
    }
}
