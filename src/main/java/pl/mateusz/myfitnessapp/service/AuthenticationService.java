package pl.mateusz.myfitnessapp.service;


import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.oauth2.core.oidc.OidcIdToken;
import org.springframework.security.oauth2.core.oidc.user.OidcUser;
import org.springframework.security.oauth2.core.user.OAuth2User;
import org.springframework.stereotype.Service;
import pl.mateusz.myfitnessapp.dto.authentication.AuthenticationResponse;
import pl.mateusz.myfitnessapp.dto.authentication.LoginRequest;
import pl.mateusz.myfitnessapp.dto.authentication.RefreshTokenRequest;
import pl.mateusz.myfitnessapp.dto.authentication.RegisterRequest;
import pl.mateusz.myfitnessapp.exceptions.AuthorizationException;
import pl.mateusz.myfitnessapp.exceptions.ApiAuthErrorCode;
import pl.mateusz.myfitnessapp.mapper.UserMapper;
import pl.mateusz.myfitnessapp.model.NotificationEmail;
import pl.mateusz.myfitnessapp.model.token.VerificationToken;
import pl.mateusz.myfitnessapp.model.userdata.User;
import pl.mateusz.myfitnessapp.repository.UserRepository;
import pl.mateusz.myfitnessapp.repository.VerificationTokenRepository;
import pl.mateusz.myfitnessapp.security.JWT.JwtProvider;

import javax.mail.MessagingException;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.Optional;
import java.util.UUID;

@Service
@AllArgsConstructor
@Slf4j
public class AuthenticationService {

    private final PasswordEncoder passwordEncoder;
    private final UserMapper userMapper;
    private final UserRepository userRepository;
    private final MailService mailService;
    private final VerificationTokenRepository verificationTokenRepository;
    private final AuthenticationManager authenticationManager;
    private final JwtProvider jwtProvider;


    public void signup(RegisterRequest registerRequest) throws MessagingException {

        User user = userMapper.map(registerRequest);
            user.setPassword(passwordEncoder.encode(registerRequest.getPassword()));

            userRepository.save(user);

            String token = generateVerificationToken(user);
            mailService.sendMail(new NotificationEmail("Please activate your Account",
                    user.getEmail(),
                    "Please click on the link to activate your account "
                            + "http://localhost:8080/api/auth/accountVerification/" + token));
    }

    private String generateVerificationToken(User user) {
        String token= UUID.randomUUID().toString();
        VerificationToken verificationToken=new VerificationToken();
        verificationToken.setToken(token);
        verificationToken.setUser(user);

        verificationTokenRepository.save(verificationToken);

        return token;
    }

    public void verifyAccount(String activationToken) {
        Optional<VerificationToken>verificationToken= verificationTokenRepository.findByToken(activationToken);
        verificationToken.orElseThrow(()->new AuthorizationException(ApiAuthErrorCode.VERIFICATION_TOKEN_NOT_FOUND));
        fetchUserAndEnable(verificationToken.get());
    }

    private void fetchUserAndEnable(VerificationToken verificationToken) {
        String userLogin=verificationToken.getUser().getUserLogin();
        User user=userRepository.findByUserLogin(userLogin).orElseThrow(()->new AuthorizationException(ApiAuthErrorCode.VERIFICATION_TOKEN_NOT_FOUND));
        user.setEnabled(true);
        userRepository.save(user);
    }


    public AuthenticationResponse login(LoginRequest loginRequest) {

//        checkIfEnabled(loginRequest.getUserLogin());

        Authentication userAuthentication=authenticationManager
                .authenticate(new UsernamePasswordAuthenticationToken(loginRequest.getEmail(),loginRequest.getPassword()));

        SecurityContextHolder.getContext().setAuthentication(userAuthentication);

        String token=jwtProvider.generateToken(userAuthentication);

        log.info("Authenticated user login"+userAuthentication.getName());
        log.info("Token: "+token);


        return AuthenticationResponse.builder()
                .username(userAuthentication.getName())
                .authenticationToken(token)
                .refreshToken(jwtProvider.generateRefreshToken(loginRequest.getEmail()))
                .expiresAt(Instant.now().plus(40, ChronoUnit.SECONDS))
                .build();
    }

    public void checkIfEnabled(String userLogin){
        userRepository.checkIfUserEnabled(userLogin).
                orElseThrow(()->new AuthorizationException(ApiAuthErrorCode.USER_NOT_ENABLED));
    }




    public AuthenticationResponse refreshToken(RefreshTokenRequest refreshTokenRequest) {

        jwtProvider.validateRefreshToken(refreshTokenRequest.getRefreshToken());
        String newToken=jwtProvider.generateTokenWithUserName(refreshTokenRequest.getUsername());

        return AuthenticationResponse.builder()
                .username(refreshTokenRequest.getUsername())
                .authenticationToken(newToken)
                .refreshToken(refreshTokenRequest.getRefreshToken())
                .expiresAt(Instant.now().plus(40, ChronoUnit.SECONDS))
                .build();
    }

    public User getCurrentUser(){

        Authentication loggedInUser = SecurityContextHolder.getContext().getAuthentication();
        String username = loggedInUser.getName();

        return userRepository.findByUserLoginOrEmail(username)
                .orElseThrow(()->new AuthorizationException(ApiAuthErrorCode.USER_NOT_FOUND));
    }
}


