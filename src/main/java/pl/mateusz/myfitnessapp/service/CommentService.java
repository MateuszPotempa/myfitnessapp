package pl.mateusz.myfitnessapp.service;

import pl.mateusz.myfitnessapp.dto.comment.CommentRequest;
import pl.mateusz.myfitnessapp.dto.comment.CommentResponse;

import java.io.IOException;

public interface CommentService extends FileTransfer {

    void createComment(CommentRequest commentRequest) throws IOException;
    void deleteComment(Long id);
    void updateComment(CommentRequest commentRequest, Long id);
    CommentResponse getCommentById(Long id);
}
