package pl.mateusz.myfitnessapp.service;

import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import pl.mateusz.myfitnessapp.dto.comment.CommentRequest;
import pl.mateusz.myfitnessapp.dto.comment.CommentResponse;
import pl.mateusz.myfitnessapp.exceptions.ApiCallException;
import pl.mateusz.myfitnessapp.exceptions.ApiErrorCode;
import pl.mateusz.myfitnessapp.mapper.CommentMapper;
import pl.mateusz.myfitnessapp.model.Comment;
import pl.mateusz.myfitnessapp.model.Post;
import pl.mateusz.myfitnessapp.model.userdata.User;
import pl.mateusz.myfitnessapp.repository.CommentRepository;
import pl.mateusz.myfitnessapp.repository.PostRepository;
import pl.mateusz.myfitnessapp.util.ImageUploadUtil;

import java.io.IOException;

@Service
@AllArgsConstructor
public class CommentServiceImpl implements CommentService {

    private final CommentRepository commentRepository;
    private final PostRepository postRepository;
    private final CommentMapper commentMapper;
    private final AuthenticationService authenticationService;
    private final ImageUploadUtil imageUploadUtil;



    @Override
    public void createComment(CommentRequest commentRequest) throws IOException {

        Post post=postRepository.findById(commentRequest.getPostId())
                .orElseThrow(()->new ApiCallException(ApiErrorCode.POST_NOT_FOUND));

        Comment comment=commentMapper.map(commentRequest,post,authenticationService.getCurrentUser());

        if(commentRequest.getImagePath()!=null) {
            comment.setImagePath(imageUploadUtil
                    .moveToTargetRepository(commentRequest.getImagePath(),
                            moveToDomainDirectory()));
        }
        commentRepository.save(comment);
    }

    @Override
    public String moveToDomainDirectory() {
        User currentUser = authenticationService.getCurrentUser();

        Long commentId=commentRepository.findFirstByOrderByIdDesc()
                .orElse(0L);

        commentId++;

        return "comment/user "+currentUser.getId()+"/comment "+commentId+"/";
    }

    @Override
    public void deleteComment(Long id) {

        Comment comment=commentRepository.findById(id)
                .orElseThrow(()->new ApiCallException(ApiErrorCode.COMMENT_NOT_FOUND));

        commentRepository.delete(comment);
    }

    @Override
    public void updateComment(CommentRequest commentRequest, Long id) {


        Comment comment=commentRepository.findById(id)
                .orElseThrow(()->new ApiCallException(ApiErrorCode.COMMENT_NOT_FOUND));

        commentMapper.updateComment(commentRequest,comment);

        commentRepository.save(comment);
    }

    public CommentResponse getCommentById(Long id) {

        return commentRepository.findById(id)
                .map(commentMapper::mapToDto)
                .orElseThrow(()->new ApiCallException(ApiErrorCode.COMMENT_NOT_FOUND));

    }


}
