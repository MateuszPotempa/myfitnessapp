package pl.mateusz.myfitnessapp.service;

import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import pl.mateusz.myfitnessapp.dto.group.UserGroupPayload;
import pl.mateusz.myfitnessapp.exceptions.ApiAuthErrorCode;
import pl.mateusz.myfitnessapp.exceptions.ApiCallException;
import pl.mateusz.myfitnessapp.exceptions.ApiErrorCode;
import pl.mateusz.myfitnessapp.exceptions.AuthorizationException;
import pl.mateusz.myfitnessapp.mapper.UserGroupMapper;
import pl.mateusz.myfitnessapp.model.groupdetails.Group;
import pl.mateusz.myfitnessapp.model.userdata.User;
import pl.mateusz.myfitnessapp.model.userdata.UserGroup;
import pl.mateusz.myfitnessapp.repository.GroupRepository;
import pl.mateusz.myfitnessapp.repository.UserGroupRepository;
import pl.mateusz.myfitnessapp.repository.UserRepository;

@Service
@AllArgsConstructor
public class GroupAdminService {


    private final GroupRepository groupRepository;
    private final UserGroupRepository userGroupRepository;
    private final UserGroupMapper userGroupMapper;
    private final UserRepository userRepository;


    public void addMemberToGroup(Long groupId, UserGroupPayload userGroupPayload) {

        Group group= groupRepository
                .findById(groupId)
                .orElseThrow(()->new ApiCallException(ApiErrorCode.GROUP_NOT_FOUND));

        User user=userRepository
                .findById(userGroupPayload.getUserId())
                .orElseThrow(()->new AuthorizationException(ApiAuthErrorCode.USER_NOT_FOUND));

        UserGroup userGroup=userGroupRepository
                .findByUserAndGroups(user,group);

        if(userGroup==null){
            userGroup=userGroupMapper.map(user,userGroupPayload.getGroupRole());
            group.addUserGroup(userGroup);
            groupRepository.save(group);
        }
    }

    public void updateMemberRole(Long groupId, UserGroupPayload userGroupPayload) {

        Group group= groupRepository
                .findById(groupId)
                .orElseThrow(()->new ApiCallException(ApiErrorCode.GROUP_NOT_FOUND));

        UserGroup userGroup=userGroupRepository
                .findById(userGroupPayload.getUserId())
                .orElseThrow(()->new AuthorizationException(ApiAuthErrorCode.USER_NOT_FOUND));

        userGroup.setGroupRole(userGroupPayload.getGroupRole());

        groupRepository.save(group);
    }
}
