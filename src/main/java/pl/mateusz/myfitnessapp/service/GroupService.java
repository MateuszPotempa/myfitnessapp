package pl.mateusz.myfitnessapp.service;

import pl.mateusz.myfitnessapp.dto.group.GroupDto;
import pl.mateusz.myfitnessapp.dto.group.GroupResponse;

import java.util.List;

public interface GroupService {

    void createGroup(GroupDto groupDto);
    GroupResponse getGroupById(Long id);
    List<String> usersWithGroupAdminAuthorities(Long groupId);
}
