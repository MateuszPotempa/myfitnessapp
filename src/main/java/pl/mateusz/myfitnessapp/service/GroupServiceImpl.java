package pl.mateusz.myfitnessapp.service;

import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import pl.mateusz.myfitnessapp.dto.group.GroupDto;
import pl.mateusz.myfitnessapp.dto.group.GroupResponse;
import pl.mateusz.myfitnessapp.exceptions.ApiCallException;
import pl.mateusz.myfitnessapp.exceptions.ApiErrorCode;
import pl.mateusz.myfitnessapp.mapper.GroupMapper;
import pl.mateusz.myfitnessapp.model.groupdetails.Group;
import pl.mateusz.myfitnessapp.model.groupdetails.GroupRole;
import pl.mateusz.myfitnessapp.model.userdata.User;
import pl.mateusz.myfitnessapp.model.userdata.UserGroup;
import pl.mateusz.myfitnessapp.repository.GroupRepository;
import pl.mateusz.myfitnessapp.repository.UserGroupRepository;


import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

@Service
@AllArgsConstructor
public class GroupServiceImpl implements GroupService {

    private final GroupRepository groupRepository;
    private final UserGroupRepository userGroupRepository;
    private final AuthenticationService authenticationService;
    private final GroupMapper groupMapper;

    @Override
    public void createGroup(GroupDto groupDto) {

        UserGroup userGroup=new UserGroup();
        userGroup.setUser(authenticationService.getCurrentUser());
        userGroup.setGroupRole(GroupRole.GROUP_OWNER);

        Group group=groupMapper.map(groupDto);
        group.addUserGroup(userGroup);

        groupRepository.save(group);
    }

    @Override
    public GroupResponse getGroupById(Long id) {
        return groupRepository.findById(id)
                .map(groupMapper::mapToDto)
                .orElseThrow(()->new ApiCallException(ApiErrorCode.GROUP_NOT_FOUND));
    }

    @Override
    public List<String> usersWithGroupAdminAuthorities(Long groupId){
        Group group= groupRepository
                .findById(groupId)
                .orElseThrow(()->new ApiCallException(ApiErrorCode.GROUP_NOT_FOUND));

        List<UserGroup> userWithAuthorities=userGroupRepository
                .findAllByGroupRoleInAndGroups(Arrays.asList(GroupRole.GROUP_ADMIN,GroupRole.GROUP_OWNER),group);

        return userWithAuthorities.stream()
                .map(UserGroup::getUser)
                .map(User::getUserLogin)
                .collect(Collectors.toList());
    }
}
