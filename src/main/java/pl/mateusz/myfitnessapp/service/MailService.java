package pl.mateusz.myfitnessapp.service;


import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.mail.MailException;
import org.springframework.mail.MailSendException;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import pl.mateusz.myfitnessapp.model.NotificationEmail;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;

@Service
@AllArgsConstructor
@Slf4j
public class MailService {

    private final JavaMailSender javaMailSender;

    @Async
    public void sendMail(NotificationEmail notificationEmail) throws MessagingException {
        MimeMessage mimeMessage = javaMailSender.createMimeMessage();
        MimeMessageHelper messageHelper = new MimeMessageHelper(mimeMessage);
        messageHelper.setFrom("myfitnessapp@email.com");
        messageHelper.setTo(notificationEmail.getRecipient());
        messageHelper.setSubject(notificationEmail.getSubject());
        messageHelper.setText(notificationEmail.getBody());

        try {
            javaMailSender.send(mimeMessage);
            log.info("Activation email sent!!");
        } catch (MailException e) {
            log.error("Exception occurred when sending mail", e);
            throw new MailSendException("Exception occurred when sending mail to " + notificationEmail.getRecipient(), e);
        }

    }
}
