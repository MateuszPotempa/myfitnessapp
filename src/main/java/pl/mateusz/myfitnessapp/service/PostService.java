package pl.mateusz.myfitnessapp.service;

import pl.mateusz.myfitnessapp.dto.comment.CommentResponse;
import pl.mateusz.myfitnessapp.dto.post.PostRequest;
import pl.mateusz.myfitnessapp.dto.post.PostResponse;

import java.io.IOException;
import java.util.List;

public interface PostService extends FileTransfer {

    void createPost(PostRequest postRequest) throws IOException;
    void deletePost(Long id);
    void updatePost(PostRequest postRequest, Long id);
    PostResponse getPostById(Long id);
    List<CommentResponse> getAllCommentsByPost(Long postId);
    CommentResponse getTopCommentByPost(Long postId);
}
