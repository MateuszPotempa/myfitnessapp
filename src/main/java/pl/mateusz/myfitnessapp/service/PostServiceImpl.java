package pl.mateusz.myfitnessapp.service;

import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import pl.mateusz.myfitnessapp.dto.comment.CommentResponse;
import pl.mateusz.myfitnessapp.dto.post.PostRequest;
import pl.mateusz.myfitnessapp.dto.post.PostResponse;
import pl.mateusz.myfitnessapp.exceptions.ApiCallException;
import pl.mateusz.myfitnessapp.exceptions.ApiErrorCode;
import pl.mateusz.myfitnessapp.mapper.CommentMapper;
import pl.mateusz.myfitnessapp.mapper.PostMapper;
import pl.mateusz.myfitnessapp.model.Post;
import pl.mateusz.myfitnessapp.model.Thread;
import pl.mateusz.myfitnessapp.model.userdata.User;
import pl.mateusz.myfitnessapp.repository.CommentRepository;
import pl.mateusz.myfitnessapp.repository.PostRepository;
import pl.mateusz.myfitnessapp.repository.ThreadRepository;
import pl.mateusz.myfitnessapp.util.ImageUploadUtil;

import java.io.IOException;
import java.util.List;
import java.util.stream.Collectors;

@Service
@AllArgsConstructor
public class PostServiceImpl implements PostService {

    private final PostRepository postRepository;
    private final CommentRepository commentRepository;
    private final PostMapper postMapper;
    private final CommentMapper commentMapper;
    private final ThreadRepository threadRepository;
    private final AuthenticationService authenticationService;
    private final ImageUploadUtil imageUploadUtil;


    @Override
    public void createPost(PostRequest postRequest) throws IOException {

        Thread thread=threadRepository.findById(postRequest.getThreadId())
                .orElseThrow(()->new ApiCallException(ApiErrorCode.THREAD_NOT_FOUND));

        Post post=postMapper.map(postRequest,thread,authenticationService.getCurrentUser());

        if(postRequest.getImageUrl()!=null) {
            post.setImagePath(imageUploadUtil
                    .moveToTargetRepository(postRequest.getImageUrl(),
                           moveToDomainDirectory()));
        }

        postRepository.save(post);
    }

    @Override
    public String moveToDomainDirectory() {
        User currentUser = authenticationService.getCurrentUser();

        Long postId=postRepository.findFirstByOrderByIdDesc()
                .orElse(0L);

        postId++;

        return "post/user "+currentUser.getId()+"/post "+postId+"/";    }

    @Override
    public void deletePost(Long id) {

        Post post=postRepository.findById(id)
                .orElseThrow(()->new ApiCallException(ApiErrorCode.POST_NOT_FOUND));

        postRepository.delete(post);
    }

    @Override
    public void updatePost(PostRequest postRequest, Long id) {

        Post post=postRepository.findById(id)
                .orElseThrow(()->new ApiCallException(ApiErrorCode.POST_NOT_FOUND));

        postMapper.updatePost(postRequest,post);

        postRepository.save(post);
    }

    @Override
    public PostResponse getPostById(Long id) {

        return postRepository.findById(id)
                .map(postMapper::mapToDto)
                .orElseThrow(()->new ApiCallException(ApiErrorCode.POST_NOT_FOUND));

    }

    @Override
    public List<CommentResponse> getAllCommentsByPost(Long postId) {
            postRepository.findById(postId)
                    .orElseThrow(()->new ApiCallException(ApiErrorCode.POST_NOT_FOUND));

            return commentRepository
                    .findAllByPostId(postId)
                    .stream()
                    .map(commentMapper::mapToDto)
                    .collect(Collectors.toList());
    }


    public CommentResponse getTopCommentByPost(Long postId) {

        postRepository.findById(postId)
                .orElseThrow(()->new ApiCallException(ApiErrorCode.POST_NOT_FOUND));

        return commentRepository
                .findTopByPostIdOrderByIdDesc(postId)
                .map(commentMapper::mapToDto)
                .orElseThrow(()->new ApiCallException(ApiErrorCode.POST_NOT_FOUND));

    }

}
