package pl.mateusz.myfitnessapp.service;

import pl.mateusz.myfitnessapp.dto.post.PostResponse;
import pl.mateusz.myfitnessapp.dto.thread.ThreadRequest;
import pl.mateusz.myfitnessapp.dto.thread.ThreadResponse;

import java.util.List;

public interface ThreadService {

   void createThread(ThreadRequest threadDto);
    void deleteThread(Long id);
    ThreadResponse getThreadById(Long id);
    List<ThreadResponse> getAllThreadsByUser(String userLogin);
    List<PostResponse> getAllPostsByThread(Long threadId);
    List<ThreadResponse> getLatestThreads(Long threadNumber);


}
