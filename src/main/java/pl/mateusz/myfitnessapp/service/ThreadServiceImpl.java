package pl.mateusz.myfitnessapp.service;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import pl.mateusz.myfitnessapp.dto.post.PostResponse;
import pl.mateusz.myfitnessapp.dto.thread.ThreadRequest;
import pl.mateusz.myfitnessapp.dto.thread.ThreadResponse;
import pl.mateusz.myfitnessapp.exceptions.ApiAuthErrorCode;
import pl.mateusz.myfitnessapp.exceptions.ApiCallException;
import pl.mateusz.myfitnessapp.exceptions.ApiErrorCode;
import pl.mateusz.myfitnessapp.exceptions.AuthorizationException;
import pl.mateusz.myfitnessapp.mapper.PostMapper;
import pl.mateusz.myfitnessapp.mapper.ThreadMapper;
import pl.mateusz.myfitnessapp.model.Thread;
import pl.mateusz.myfitnessapp.repository.PostRepository;
import pl.mateusz.myfitnessapp.repository.ThreadRepository;
import pl.mateusz.myfitnessapp.repository.UserRepository;

import java.util.List;
import java.util.stream.Collectors;

@Service
@AllArgsConstructor
@Slf4j
public class ThreadServiceImpl implements ThreadService {

    private final ThreadMapper threadMapper;
    private final ThreadRepository threadRepository;
    private final PostRepository postRepository;
    private final AuthenticationService authenticationService;
    private final ThreadTagService threadTagService;
    private final UserRepository userRepository;
    private final PostMapper postMapper;


    public void createThread(ThreadRequest threadDto) {

        Thread thread=threadMapper.map(threadDto,authenticationService.getCurrentUser());

        threadTagService
                .retrieveTagsFromDto(threadDto,authenticationService.getCurrentUser(),thread);

    }

    public void deleteThread(Long id) {

        Thread thread=threadRepository.findById(id)
                .orElseThrow(()->new ApiCallException(ApiErrorCode.THREAD_NOT_FOUND));

        threadTagService.reduceTagCount(thread);

        threadRepository.delete(thread);
    }

    public ThreadResponse getThreadById(Long id) {
        return threadRepository.findById(id)
                .map(threadMapper::mapToDto)
                .orElseThrow(()->new ApiCallException(ApiErrorCode.THREAD_NOT_FOUND));
    }

    public List<ThreadResponse> getAllThreadsByUser(String userLogin) {

        userRepository.findByUserLogin(userLogin)
                .orElseThrow(()->new AuthorizationException(ApiAuthErrorCode.USER_NOT_FOUND));

        return threadRepository
                .findAllByUser_UserLogin(userLogin)
                .stream()
                .map(threadMapper::mapToDto)
                .collect(Collectors.toList());
    }

    public List<PostResponse> getAllPostsByThread(Long threadId) {

        threadRepository.findById(threadId)
                .orElseThrow(()->new ApiCallException(ApiErrorCode.THREAD_NOT_FOUND));

                return  postRepository
                        .findAllByThreadId(threadId)
                        .stream()
                        .map(postMapper::mapToDto)
                        .collect(Collectors.toList());
    }

    public List<ThreadResponse> getLatestThreads(Long threadNumber) {

        return threadRepository
                .findAllByOrderByIdDesc()
                .stream()
                .limit(threadNumber)
                .map(threadMapper::mapToDto)
                .collect(Collectors.toList());
    }
}
