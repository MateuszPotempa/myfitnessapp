package pl.mateusz.myfitnessapp.service;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import pl.mateusz.myfitnessapp.dto.thread.ThreadRequest;
import pl.mateusz.myfitnessapp.exceptions.ApiCallException;
import pl.mateusz.myfitnessapp.exceptions.ApiErrorCode;
import pl.mateusz.myfitnessapp.model.*;
import pl.mateusz.myfitnessapp.model.Thread;
import pl.mateusz.myfitnessapp.model.userdata.User;
import pl.mateusz.myfitnessapp.repository.TagCountRepository;
import pl.mateusz.myfitnessapp.repository.ThreadTagRepository;
import pl.mateusz.myfitnessapp.repository.UserTagCountRepository;

import java.util.HashSet;
import java.util.Set;

@Service
@AllArgsConstructor
@Slf4j
public class ThreadTagService {

    private final ThreadTagRepository threadTagRepository;
    private final TagCountRepository tagCountRepository;
    private final UserTagCountRepository userTagCountRepository;


    public void retrieveTagsFromDto(ThreadRequest threadDto, User user,Thread thread){

        Set<ThreadTag>preparedTags=new HashSet<>();

        threadDto.getTags().forEach(tag->{

            ThreadTag threadTags=new ThreadTag();
            threadTags.setTag(tag);
            threadTags.setThread(thread);
            threadTags.setUser(user);

            tagCountVerification(tag);
            userTagCountVerification(user,tag);


            preparedTags.add(threadTags);

        });

        threadTagRepository.saveAll(preparedTags);

    }



    public void reduceTagCount(Thread thread){

        thread.getTags().forEach(tag->{

            UserTagCount byTagAndUser = userTagCountRepository
                    .findByTagAndUser(tag.getTag(), thread.getUser())
                    .orElseThrow(()->new ApiCallException(ApiErrorCode.TAG_NOT_FOUND));
            byTagAndUser.setUserTagCount(byTagAndUser.getUserTagCount()-1);

            userTagCountRepository.save(byTagAndUser);

            TagCount tagCount= tagCountRepository.findByTag(tag.getTag());
            tagCount.setTagCount(tagCount.getTagCount()-1);

            tagCountRepository.save(tagCount);

        });

    }


    private void tagCountVerification(String tag){

      TagCount tagCount = tagCountRepository.findByTag(tag);

        if(tagCount!=null){
            tagCount.setTagCount(tagCount.getTagCount()+1);
        }
        else {
            tagCount = new TagCount();
            tagCount.setTag(tag);
            tagCount.setTagCount(1);
        }

        tagCountRepository.save(tagCount);

    }

    public void userTagCountVerification(User user, String tag){

        UserTagCount userTagCount=userTagCountRepository
                .findByTagAndUser(tag,user)
                .orElseThrow(()->new ApiCallException(ApiErrorCode.TAG_NOT_FOUND));;

        if(userTagCount!=null){
            userTagCount.setUserTagCount(userTagCount.getUserTagCount()+1);
        }
        else{
            userTagCount=new UserTagCount();
            userTagCount.setUser(user);
            userTagCount.setTag(tag);
            userTagCount.setUserTagCount(1);
        }

        userTagCountRepository.save(userTagCount);

    }

    private String prepareTag(String tag){
        tag=tag.trim().replaceAll("\\s+", "").toLowerCase().substring(1);
        return tag;
    }

}
