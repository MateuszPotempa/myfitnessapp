package pl.mateusz.myfitnessapp.service;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import pl.mateusz.myfitnessapp.model.userdata.User;
import pl.mateusz.myfitnessapp.repository.UserRepository;

import java.util.Optional;

@Service
@AllArgsConstructor
@Slf4j
public class UserDetailsServiceImpl implements UserDetailsService {

    private final UserRepository userRepository;

    @Override
    public UserDetails loadUserByUsername(String userLoginOrEmail) throws UsernameNotFoundException {

        Optional<User> presentUser= Optional.ofNullable(userRepository
                .findByUserLoginOrEmail(userLoginOrEmail)
                .orElseThrow(() -> new UsernameNotFoundException("User " + userLoginOrEmail + " not found!")));

        log.info("2. User login option inside UserDetailsService "+userLoginOrEmail);
        return UserPrincipal.create(presentUser.get());
    }
}
