package pl.mateusz.myfitnessapp.util;

import lombok.NoArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Objects;

import static java.nio.file.StandardCopyOption.REPLACE_EXISTING;

@Component
@NoArgsConstructor
public class ImageUploadUtil {

    @Value("${file.directory}")
    private String directory;


    public String saveImage(MultipartFile multipartFile) throws IOException {

        return saveFile(multipartFile);
    }

    private String saveFile(MultipartFile multipartFile) throws IOException {

        String fileName = StringUtils.cleanPath(Objects.requireNonNull(multipartFile.getOriginalFilename()));

        Path uploadPath = Paths.get(directory);

        if (!Files.exists(uploadPath)) {
            Files.createDirectories(uploadPath);
        }

        Path filePath = uploadPath.resolve(fileName);

        try (InputStream inputStream = multipartFile.getInputStream()) {
            Files.copy(inputStream, filePath, REPLACE_EXISTING);

        } catch (IOException ioe) {
            throw new IOException("Could not save image file: " + fileName, ioe);
        }

        return fileName;

    }

    public String moveToTargetRepository(String fileName, String targetSource) throws IOException {

        Path targetSourcePath=Paths.get(targetSource);

        Path sourcePath=Paths.get(directory + "/" + fileName);

        if (!Files.exists(targetSourcePath)) {
            Files.createDirectories(targetSourcePath);
        }

        Path targetPath = targetSourcePath.resolve(fileName);

        Files.move(sourcePath,
                        targetPath,
                        REPLACE_EXISTING);

        return fileName;
    }
}
