package pl.mateusz.myfitnessapp.util;

import javax.script.Invocable;
import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;
import javax.script.ScriptException;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;

public class RunJsScript_WIP {

    public void connectToWebSocket() {

        ScriptEngineManager manager = new ScriptEngineManager();
        ScriptEngine engine = manager.getEngineByName("JavaScript");

        try {
            engine.eval(Files.newBufferedReader(Paths.get("/static/app.js"), StandardCharsets.UTF_8));

            Invocable inv = (Invocable) engine;
            inv.invokeFunction("connect()", "");

        } catch (IOException|ScriptException|NoSuchMethodException e){
            e.printStackTrace();
        }
    }
}

