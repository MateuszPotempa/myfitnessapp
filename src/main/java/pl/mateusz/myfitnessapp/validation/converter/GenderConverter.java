package pl.mateusz.myfitnessapp.validation.converter;

import org.springframework.stereotype.Component;
import pl.mateusz.myfitnessapp.model.groupdetails.GroupRole;
import pl.mateusz.myfitnessapp.model.userdata.Gender;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;
import java.util.Optional;

@Component
@Converter(autoApply = true)
public class GenderConverter implements AttributeConverter<Gender,String> {


    @Override
    public String convertToDatabaseColumn(Gender gender) {
        return Optional.ofNullable(gender).map(Gender::getGenderType).orElse(null);
    }

    @Override
    public Gender convertToEntityAttribute(String gender) {
        return Gender.decode(gender);
    }
}
