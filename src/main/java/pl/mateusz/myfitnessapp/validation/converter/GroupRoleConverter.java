package pl.mateusz.myfitnessapp.validation.converter;

import org.springframework.stereotype.Component;
import pl.mateusz.myfitnessapp.model.groupdetails.GroupRole;
import pl.mateusz.myfitnessapp.model.groupdetails.GroupType;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;
import java.util.Optional;

@Component
@Converter(autoApply = true)
public class GroupRoleConverter implements AttributeConverter<GroupRole, String> {


    @Override
    public String convertToDatabaseColumn(GroupRole attribute) {
        return Optional.ofNullable(attribute).map(GroupRole::getRole).orElse(null);
    }

    @Override
    public GroupRole convertToEntityAttribute(String repoData) {
        return GroupRole.decode(repoData);
    }
}
