package pl.mateusz.myfitnessapp.validation.converter;

import org.springframework.stereotype.Component;
import pl.mateusz.myfitnessapp.model.groupdetails.GroupType;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;
import java.util.Optional;

@Component
@Converter(autoApply = true)
public class GroupTypeConverter implements AttributeConverter<GroupType, String> {


    @Override
    public String convertToDatabaseColumn(GroupType attribute) {
        return Optional.ofNullable(attribute).map(GroupType::getType).orElse(null);
    }

    @Override
        public GroupType convertToEntityAttribute(final String repoData) {
            return GroupType.decode(repoData);
        }
    }



