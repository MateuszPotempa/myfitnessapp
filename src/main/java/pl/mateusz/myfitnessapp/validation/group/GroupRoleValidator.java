package pl.mateusz.myfitnessapp.validation.group;

import pl.mateusz.myfitnessapp.model.groupdetails.GroupRole;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class GroupRoleValidator implements ConstraintValidator<GroupRoleValidation, GroupRole> {
    @Override
    public void initialize(GroupRoleValidation constraintAnnotation) {
    }

    @Override
    public boolean isValid(GroupRole groupRole, ConstraintValidatorContext context) {
        return !groupRole.getRole().equals(GroupRole.GROUP_OWNER.getRole());
    }
}
