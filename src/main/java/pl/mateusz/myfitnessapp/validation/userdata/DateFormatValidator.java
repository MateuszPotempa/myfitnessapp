package pl.mateusz.myfitnessapp.validation.userdata;

import org.springframework.stereotype.Component;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

@Component
public class DateFormatValidator implements ConstraintValidator<DateFormatConstraint, String> {


    @Override
    public void initialize(DateFormatConstraint constraintAnnotation) {
    }

    @Override
    public boolean isValid(String date, ConstraintValidatorContext context) {
        DateTimeFormatter format = DateTimeFormatter.ofPattern("yyyy-MM-dd");
        if (date==null||!date.matches("\\d{4}-[01]\\d-[0-3]\\d"))
            return false;
        try {
            LocalDate.parse(date,format);
            return true;
        } catch(Exception e) {
            return false;
        }

    }
}