package pl.mateusz.myfitnessapp.validation.userdata;

import lombok.AllArgsConstructor;
import org.springframework.stereotype.Component;

import pl.mateusz.myfitnessapp.repository.UserRepository;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

@Component
@AllArgsConstructor
public class EmailExistConstraintValidator implements ConstraintValidator<EmailExistConstraint,String> {

    private final UserRepository userRepository;

    @Override
    public void initialize(EmailExistConstraint constraintAnnotation) {

    }

    @Override
    public boolean isValid(String email, ConstraintValidatorContext context) {

        return !userRepository.existsByEmail(email);
    }
}
