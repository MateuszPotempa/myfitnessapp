package pl.mateusz.myfitnessapp.validation.userdata;

import lombok.AllArgsConstructor;
import org.springframework.stereotype.Component;
import pl.mateusz.myfitnessapp.repository.UserRepository;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

@Component
@AllArgsConstructor
public class LoginExistConstraintValidator implements ConstraintValidator<LoginExistConstraint,String> {

    private final UserRepository userRepository;

    @Override
    public void initialize(LoginExistConstraint constraintAnnotation) {

    }

    @Override
    public boolean isValid(String userLogin, ConstraintValidatorContext context) {

        return !userRepository.existsByUserLogin(userLogin);
    }
}
