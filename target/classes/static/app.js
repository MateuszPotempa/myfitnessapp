var stompClient = null;

function setConnected(connected) {
    $("#connect").prop("disabled", connected);
    $("#disconnect").prop("disabled", !connected);
    if (connected) {
        $("#conversation").show();
    }
    else {
        $("#conversation").hide();
    }
    $("#greetings").html("");
}

function connect() {
    var socket = new SockJS('/groups');
    stompClient = Stomp.over(socket);
    stompClient.connect({'X-Authorization': 'eyJhbGciOiJSUzI1NiJ9.eyJzdWIiOiJlbXBzaTE2IiwiaWF0IjoxNjA4MTQyNzM5LCJleHAiOjE2MDkxNDI3Mzl9.Oa1EHbk1PCnuxlJjuk5SkRNIjRtOWRk5W9s7PFoTizJFNO3Vd7WB7zAQkDy4vk4SD6qA2osD-HBHOrBCcjh-GwBCgWPAdBT2uZUX8ASnZitPQH4URLBbfG4m6rzZf3iVC9wPcjhpJWI5A8T5qApA4_2gin-wFQzciX0ppJPxpV1Mjb6W0Crz3-ck4QfrL6gOssaHuKmfZckcuFuxhMlPSkM3ycjcPgQT0dv5Zc6Xq45AyJHQb3ng7TWgi8-s4l1qWYZuvPqEY2D29lE-Vu5F7GaDA1dK9TUlk-5vs1qHEA1lWeL45cRDKz4gWrzRa7R7YuA4oGf2ABqJXTHOtHO3Mg'}, function (frame) {
        setConnected(true);
        console.log('Connected: ' + frame);
        stompClient.subscribe('/user/queue/notifications', function (notification) {
            console.log(notification)
            showNotification(notification.body);
        });
    });
}

$(function () {
    $("form").on('submit', function (e) {
        e.preventDefault();
    });
    $( "#connect" ).click(function() { connect(); });
    $( "#disconnect" ).click(function() { disconnect(); });
    $( "#send" ).click(function() { sendName(); });
});

function showNotification(notification) {

    var obj=JSON.parse(notification)
    $("#notifications").
    append("<tr><td>" +obj.senderUserName+" wants to "+ obj.message + "</td></tr>");
}

function disconnect() {
    if (stompClient !== null) {
        stompClient.disconnect();
    }
    setConnected(false);
    console.log("Disconnected");
}


// function sendNotification() {
//     stompClient.send("/app/hello", {}, JSON.stringify({'name': $("#name").val()}));
// }

