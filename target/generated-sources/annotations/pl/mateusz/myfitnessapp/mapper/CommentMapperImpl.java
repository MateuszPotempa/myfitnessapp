package pl.mateusz.myfitnessapp.mapper;

import javax.annotation.processing.Generated;
import org.springframework.stereotype.Component;
import pl.mateusz.myfitnessapp.dto.comment.CommentRequest;
import pl.mateusz.myfitnessapp.dto.comment.CommentResponse;
import pl.mateusz.myfitnessapp.model.Comment;
import pl.mateusz.myfitnessapp.model.Post;
import pl.mateusz.myfitnessapp.model.userdata.User;

@Generated(
    value = "org.mapstruct.ap.MappingProcessor",
    date = "2021-02-10T00:33:17+0100",
    comments = "version: 1.4.0.Final, compiler: javac, environment: Java 15.0.2 (Oracle Corporation)"
)
@Component
public class CommentMapperImpl implements CommentMapper {

    @Override
    public Comment map(CommentRequest commentRequest, Post post, User user) {
        if ( commentRequest == null && post == null && user == null ) {
            return null;
        }

        Comment comment = new Comment();

        if ( commentRequest != null ) {
            comment.setContent( commentRequest.getContent() );
        }
        if ( post != null ) {
            comment.setPost( post );
        }
        if ( user != null ) {
            comment.setUser( user );
        }
        comment.setCreationDate( java.time.Instant.now() );

        return comment;
    }

    @Override
    public CommentResponse mapToDto(Comment comment) {
        if ( comment == null ) {
            return null;
        }

        CommentResponse commentResponse = new CommentResponse();

        commentResponse.setContent( comment.getContent() );
        commentResponse.setCreatedDate( comment.getCreationDate() );
        commentResponse.setCommentOwner( commentUserUserLogin( comment ) );
        commentResponse.setImagePath( comment.getImagePath() );

        commentResponse.setEdited( checkEdition(comment) );

        return commentResponse;
    }

    @Override
    public void updateComment(CommentRequest commentRequest, Comment comment) {
        if ( commentRequest == null ) {
            return;
        }

        if ( commentRequest.getContent() != null ) {
            comment.setContent( commentRequest.getContent() );
        }
        if ( commentRequest.getImagePath() != null ) {
            comment.setImagePath( commentRequest.getImagePath() );
        }

        comment.setEditionDate( java.time.Instant.now() );
    }

    private String commentUserUserLogin(Comment comment) {
        if ( comment == null ) {
            return null;
        }
        User user = comment.getUser();
        if ( user == null ) {
            return null;
        }
        String userLogin = user.getUserLogin();
        if ( userLogin == null ) {
            return null;
        }
        return userLogin;
    }
}
