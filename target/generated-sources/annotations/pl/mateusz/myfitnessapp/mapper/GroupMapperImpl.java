package pl.mateusz.myfitnessapp.mapper;

import javax.annotation.processing.Generated;
import org.springframework.stereotype.Component;
import pl.mateusz.myfitnessapp.dto.group.GroupDto;
import pl.mateusz.myfitnessapp.dto.group.GroupResponse;
import pl.mateusz.myfitnessapp.model.groupdetails.Group;
import pl.mateusz.myfitnessapp.model.groupdetails.GroupInfo;
import pl.mateusz.myfitnessapp.model.groupdetails.GroupType;

@Generated(
    value = "org.mapstruct.ap.MappingProcessor",
    date = "2021-02-10T00:33:17+0100",
    comments = "version: 1.4.0.Final, compiler: javac, environment: Java 15.0.2 (Oracle Corporation)"
)
@Component
public class GroupMapperImpl extends GroupMapper {

    @Override
    public Group map(GroupDto groupDto) {
        if ( groupDto == null ) {
            return null;
        }

        Group group = new Group();

        group.setInfo( groupDtoToGroupInfo( groupDto ) );
        group.setName( groupDto.getName() );

        return group;
    }

    @Override
    public GroupResponse mapToDto(Group group) {
        if ( group == null ) {
            return null;
        }

        GroupResponse groupResponse = new GroupResponse();

        groupResponse.setName( group.getName() );
        groupResponse.setPolicy( groupInfoPolicy( group ) );
        groupResponse.setGroupType( groupInfoGroupType( group ) );
        groupResponse.setDescription( groupInfoDescription( group ) );
        groupResponse.setGroupId( group.getId() );
        groupResponse.setIsVisible( groupInfoIsVisible( group ) );

        groupResponse.setGroupOwner( getOwnerName(group) );

        return groupResponse;
    }

    protected GroupInfo groupDtoToGroupInfo(GroupDto groupDto) {
        if ( groupDto == null ) {
            return null;
        }

        GroupInfo groupInfo = new GroupInfo();

        groupInfo.setDescription( groupDto.getDescription() );
        groupInfo.setGroupType( groupDto.getGroupType() );
        groupInfo.setIsVisible( groupDto.getIsVisible() );
        groupInfo.setPolicy( groupDto.getPolicy() );

        return groupInfo;
    }

    private String groupInfoPolicy(Group group) {
        if ( group == null ) {
            return null;
        }
        GroupInfo info = group.getInfo();
        if ( info == null ) {
            return null;
        }
        String policy = info.getPolicy();
        if ( policy == null ) {
            return null;
        }
        return policy;
    }

    private GroupType groupInfoGroupType(Group group) {
        if ( group == null ) {
            return null;
        }
        GroupInfo info = group.getInfo();
        if ( info == null ) {
            return null;
        }
        GroupType groupType = info.getGroupType();
        if ( groupType == null ) {
            return null;
        }
        return groupType;
    }

    private String groupInfoDescription(Group group) {
        if ( group == null ) {
            return null;
        }
        GroupInfo info = group.getInfo();
        if ( info == null ) {
            return null;
        }
        String description = info.getDescription();
        if ( description == null ) {
            return null;
        }
        return description;
    }

    private Boolean groupInfoIsVisible(Group group) {
        if ( group == null ) {
            return null;
        }
        GroupInfo info = group.getInfo();
        if ( info == null ) {
            return null;
        }
        Boolean isVisible = info.getIsVisible();
        if ( isVisible == null ) {
            return null;
        }
        return isVisible;
    }
}
