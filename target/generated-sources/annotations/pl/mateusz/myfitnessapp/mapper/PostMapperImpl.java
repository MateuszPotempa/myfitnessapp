package pl.mateusz.myfitnessapp.mapper;

import javax.annotation.processing.Generated;
import org.springframework.stereotype.Component;
import pl.mateusz.myfitnessapp.dto.post.PostRequest;
import pl.mateusz.myfitnessapp.dto.post.PostResponse;
import pl.mateusz.myfitnessapp.model.Post;
import pl.mateusz.myfitnessapp.model.Thread;
import pl.mateusz.myfitnessapp.model.userdata.User;

@Generated(
    value = "org.mapstruct.ap.MappingProcessor",
    date = "2021-02-10T00:33:17+0100",
    comments = "version: 1.4.0.Final, compiler: javac, environment: Java 15.0.2 (Oracle Corporation)"
)
@Component
public class PostMapperImpl implements PostMapper {

    @Override
    public Post map(PostRequest postRequest, Thread thread, User user) {
        if ( postRequest == null && thread == null && user == null ) {
            return null;
        }

        Post post = new Post();

        if ( postRequest != null ) {
            post.setPostName( postRequest.getPostName() );
            post.setContent( postRequest.getContent() );
        }
        if ( thread != null ) {
            post.setThread( thread );
        }
        if ( user != null ) {
            post.setUser( user );
        }
        post.setCreatedDate( java.time.Instant.now() );

        return post;
    }

    @Override
    public PostResponse mapToDto(Post post) {
        if ( post == null ) {
            return null;
        }

        PostResponse postResponse = new PostResponse();

        postResponse.setContent( post.getContent() );
        postResponse.setPostName( post.getPostName() );
        postResponse.setCreatedDate( post.getCreatedDate() );
        postResponse.setThreadId( postThreadId( post ) );
        postResponse.setUserOwner( postUserUserLogin( post ) );
        postResponse.setImageUrl( post.getImagePath() );

        return postResponse;
    }

    @Override
    public void updatePost(PostRequest postRequest, Post post) {
        if ( postRequest == null ) {
            return;
        }

        if ( postRequest.getPostName() != null ) {
            post.setPostName( postRequest.getPostName() );
        }
        if ( postRequest.getContent() != null ) {
            post.setContent( postRequest.getContent() );
        }
    }

    private Long postThreadId(Post post) {
        if ( post == null ) {
            return null;
        }
        Thread thread = post.getThread();
        if ( thread == null ) {
            return null;
        }
        Long id = thread.getId();
        if ( id == null ) {
            return null;
        }
        return id;
    }

    private String postUserUserLogin(Post post) {
        if ( post == null ) {
            return null;
        }
        User user = post.getUser();
        if ( user == null ) {
            return null;
        }
        String userLogin = user.getUserLogin();
        if ( userLogin == null ) {
            return null;
        }
        return userLogin;
    }
}
