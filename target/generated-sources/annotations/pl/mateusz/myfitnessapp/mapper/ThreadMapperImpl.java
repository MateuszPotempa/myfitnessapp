package pl.mateusz.myfitnessapp.mapper;

import javax.annotation.processing.Generated;
import org.springframework.stereotype.Component;
import pl.mateusz.myfitnessapp.dto.thread.ThreadRequest;
import pl.mateusz.myfitnessapp.dto.thread.ThreadResponse;
import pl.mateusz.myfitnessapp.model.Thread;
import pl.mateusz.myfitnessapp.model.userdata.User;

@Generated(
    value = "org.mapstruct.ap.MappingProcessor",
    date = "2021-02-10T00:33:17+0100",
    comments = "version: 1.4.0.Final, compiler: javac, environment: Java 15.0.2 (Oracle Corporation)"
)
@Component
public class ThreadMapperImpl implements ThreadMapper {

    @Override
    public Thread map(ThreadRequest threadDto, User user) {
        if ( threadDto == null && user == null ) {
            return null;
        }

        Thread thread = new Thread();

        if ( threadDto != null ) {
            thread.setThreadName( threadDto.getThreadName() );
            thread.setDescription( threadDto.getDescription() );
        }
        if ( user != null ) {
            thread.setUser( user );
        }
        thread.setCreatedDate( java.time.Instant.now() );

        return thread;
    }

    @Override
    public ThreadResponse mapToDto(Thread thread) {
        if ( thread == null ) {
            return null;
        }

        ThreadResponse threadResponse = new ThreadResponse();

        threadResponse.setDescription( thread.getDescription() );
        threadResponse.setThreadName( thread.getThreadName() );
        threadResponse.setThreadOwner( threadUserUserLogin( thread ) );

        threadResponse.setNumberOfPosts( mapPosts(thread.getPosts()) );
        threadResponse.setThreadTags( mapTags(thread.getTags()) );

        return threadResponse;
    }

    private String threadUserUserLogin(Thread thread) {
        if ( thread == null ) {
            return null;
        }
        User user = thread.getUser();
        if ( user == null ) {
            return null;
        }
        String userLogin = user.getUserLogin();
        if ( userLogin == null ) {
            return null;
        }
        return userLogin;
    }
}
