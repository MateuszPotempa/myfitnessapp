package pl.mateusz.myfitnessapp.mapper;

import javax.annotation.processing.Generated;
import org.springframework.stereotype.Component;
import pl.mateusz.myfitnessapp.model.groupdetails.GroupRole;
import pl.mateusz.myfitnessapp.model.userdata.User;
import pl.mateusz.myfitnessapp.model.userdata.UserGroup;

@Generated(
    value = "org.mapstruct.ap.MappingProcessor",
    date = "2021-02-10T00:33:17+0100",
    comments = "version: 1.4.0.Final, compiler: javac, environment: Java 15.0.2 (Oracle Corporation)"
)
@Component
public class UserGroupMapperImpl implements UserGroupMapper {

    @Override
    public UserGroup map(User user, GroupRole groupRole) {
        if ( user == null && groupRole == null ) {
            return null;
        }

        UserGroup userGroup = new UserGroup();

        if ( user != null ) {
            userGroup.setUser( user );
        }
        if ( groupRole != null ) {
            userGroup.setGroupRole( groupRole );
        }

        return userGroup;
    }
}
