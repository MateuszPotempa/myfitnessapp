package pl.mateusz.myfitnessapp.mapper;

import javax.annotation.processing.Generated;
import org.springframework.stereotype.Component;
import pl.mateusz.myfitnessapp.dto.authentication.RegisterRequest;
import pl.mateusz.myfitnessapp.model.token.AuthProvider;
import pl.mateusz.myfitnessapp.model.userdata.PersonalInformation;
import pl.mateusz.myfitnessapp.model.userdata.User;
import pl.mateusz.myfitnessapp.security.oauth2.user.OAuth2UserInfo;

@Generated(
    value = "org.mapstruct.ap.MappingProcessor",
    date = "2021-02-16T23:50:15+0100",
    comments = "version: 1.4.0.Final, compiler: javac, environment: Java 15.0.2 (Oracle Corporation)"
)
@Component
public class UserMapperImpl implements UserMapper {

    @Override
    public User map(RegisterRequest registerRequest) {
        if ( registerRequest == null ) {
            return null;
        }

        User user = new User();

        user.setPersonalInformation( registerRequestToPersonalInformation( registerRequest ) );
        user.setUserLogin( registerRequest.getUserLogin() );
        user.setEmail( registerRequest.getEmail() );

        user.setEnabled( false );
        user.setAuthProvider( AuthProvider.local );
        user.setDateOfCreation( java.sql.Timestamp.from(java.time.Instant.now()) );

        return user;
    }

    @Override
    public User oAuth2MapToDao(AuthProvider authProvider, OAuth2UserInfo oAuth2UserInfo) {
        if ( authProvider == null && oAuth2UserInfo == null ) {
            return null;
        }

        User user = new User();

        if ( authProvider != null ) {
            user.setAuthProvider( authProvider );
        }
        if ( oAuth2UserInfo != null ) {
            user.setPersonalInformation( oAuth2UserInfoToPersonalInformation( oAuth2UserInfo ) );
            user.setEmail( oAuth2UserInfo.getEmail() );
            user.setImageUrl( oAuth2UserInfo.getImageUrl() );
            user.setProviderId( oAuth2UserInfo.getId() );
        }
        user.setEnabled( true );
        user.setDateOfCreation( java.sql.Timestamp.from(java.time.Instant.now()) );

        return user;
    }

    protected PersonalInformation registerRequestToPersonalInformation(RegisterRequest registerRequest) {
        if ( registerRequest == null ) {
            return null;
        }

        PersonalInformation personalInformation = new PersonalInformation();

        personalInformation.setFirstName( registerRequest.getFirstName() );
        personalInformation.setGender( registerRequest.getGender() );
        personalInformation.setLastName( registerRequest.getLastName() );
        personalInformation.setDateOfBirth( registerRequest.getDateOfBirth() );
        personalInformation.setPhoneNumber( registerRequest.getPhoneNumber() );

        return personalInformation;
    }

    protected PersonalInformation oAuth2UserInfoToPersonalInformation(OAuth2UserInfo oAuth2UserInfo) {
        if ( oAuth2UserInfo == null ) {
            return null;
        }

        PersonalInformation personalInformation = new PersonalInformation();

        personalInformation.setFirstName( oAuth2UserInfo.getFirstName() );
        personalInformation.setLastName( oAuth2UserInfo.getLastName() );

        return personalInformation;
    }
}
